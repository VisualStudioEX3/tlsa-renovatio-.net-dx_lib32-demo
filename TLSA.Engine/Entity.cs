﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TLSA.Engine.Core;
using TLSA.Engine.Core.Graphics;

namespace TLSA.Engine
{
    /// <summary>
    /// Game Object. Base class for all entities in the scene.
    /// </summary>
    public class Entity
    {
        #region Static class
        #region Methods & functions
        /// <summary>
        /// Find a Entity in the scene by name.
        /// </summary>
        /// <param name="name">Name of the Entity.</param>
        /// <returns>Return the first Entity that match by name.</returns>
        public static Entity Find(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Find a Entity in the scene by tag.
        /// </summary>
        /// <param name="name">Tag of the Entity.</param>
        /// <returns>Return the first Entity that match by tag.</returns>
        public static Entity FindByTag(string tag)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Find a Entity in the scene by type.
        /// </summary>
        /// <param name="name">Type of the component attached to Entity.</param>
        /// <returns>Return the first Entity that has component that match by type.</returns>
        public static Entity FindByType<T>()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Destroy a Entity instance.
        /// </summary>
        /// <param name="instance">Instance of the Entity.</param>
        /// <param name="delay">Delay time for destroy the instance. Use 0 to destory inmediately.</param>
        public static void Destroy(Entity instance, float delay = 0f)
        {
            // Maybe try implement a pool system.

            // Call event:
            if (instance.OnDestroy != null)
            {
                instance.OnDestroy();
            }
        }
        #endregion
        #endregion

        #region Instance class
        #region Internal vars
        bool _enable;
        bool _visible;
        internal InvokeManager InvokeManager;
        #endregion

        #region Properties
        /// <summary>
        /// Unique id for this Entity.
        /// </summary>
        public Guid Id { get; private set; }

        /// <summary>
        /// Name used by this instance.
        /// </summary>
        /// <remarks>This value no is the unique id of the instance.</remarks>
        public string Name { get; set; }

        /// <summary>
        /// Enable or disabled this instance.
        /// </summary>
        /// <remarks>This affect to invoked methods with delay to start.</remarks>
        public bool Enable
        {
            get
            {
                return this._enable;
            }
            set
            {
                this._enable = this.InvokeManager.Enabled = value;
                if (this._enable)
                {
                    if (this.OnEnable != null)
                    {
                        this.OnEnable();
                    }
                }
                else
                {
                    if (this.OnDisable != null)
                    {
                        this.OnDisable();
                    }
                }
            }
        }

        /// <summary>
        /// Enable or disabled this instance.
        /// </summary>
        public bool Visible
        {
            get
            {
                return this._visible;
            }
            set
            {
                this._visible = value;
                if (this._visible)
                {
                    if (this.OnVisible != null)
                    {
                        this.OnVisible();
                    }
                }
                else
                {
                    if (this.OnNotVisible != null)
                    {
                        this.OnNotVisible();
                    }
                }
            }
        }

        /// <summary>
        /// Components attached to this Entity.
        /// </summary>
        public ComponentPool Components { get; private set; }

        /// <summary>
        /// Execution order priority.
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Father Entity. Null if not have.
        /// </summary>
        public Entity Parent { get; set; }

        /// <summary>
        /// Child Entities.
        /// </summary>
        public List<Entity> Childrens { get; private set; }

        /// <summary>
        /// Entity Tag.
        /// </summary>
        public string Tag { get; set; }

        /// <summary>
        /// Entity Layer.
        /// </summary>
        public int Layer { get; set; }

        /// <summary>
        /// Position of Entity. (Probably change this property in the future with a Transform object)
        /// </summary>
        public Point2D Position { get; private set; }

        /// <summary>
        /// Sprite attached to this Entity (need testing)
        /// </summary>
        public Sprite Sprite { get; private set; }
        #endregion

        #region Delegates
        public delegate void OnDestroyEntityHandler();
        public delegate void OnEnableEntityHandler();
        public delegate void OnDisableEntityHandler();
        public delegate void OnVisibleEntityHandler();
        public delegate void OnNotVisibleEntityHandler();

        /// <summary>
        /// Event raised when this Entity is destroyed.
        /// </summary>
        public OnDestroyEntityHandler OnDestroy;

        /// <summary>
        /// Event raised when this Entity is enabled.
        /// </summary>
        public OnEnableEntityHandler OnEnable;

        /// <summary>
        /// Event raised when this Entity is disabled.
        /// </summary>
        public OnDisableEntityHandler OnDisable;

        /// <summary>
        /// Event raised when this Entity is set visible.
        /// </summary>
        public OnVisibleEntityHandler OnVisible;

        /// <summary>
        /// Event raised when this Entity is set not visible.
        /// </summary>
        public OnNotVisibleEntityHandler OnNotVisible;
        #endregion

        #region Constructor
        public Entity()
        {
            this.Id = Guid.NewGuid();

            this.InvokeManager = new InvokeManager(this);
            this.Components = new ComponentPool();
            this.Childrens = new List<Entity>();
        }
        #endregion

        #region Methods & functions
        /// <summary>
        /// Compare with a tag.
        /// </summary>
        /// <param name="tag">Tag to compare.</param>
        /// <returns>Return true if the both tags are equals.</returns>
        /// <remarks>This method if more optimized for string comparasions than the == and != operators.
        /// Reference: https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/strings/how-to-compare-strings.
        /// </remarks>
        public bool CompareTag(string tag)
        {
            return this.Tag.Equals(tag, StringComparison.Ordinal);
        }

        /// <summary>
        /// Add a component to the Entity.
        /// </summary>
        /// <typeparam name="T">The type of the component to add.</typeparam>
        /// <returns>Return the new component instance.</returns>
        public T AddComponent<T>() where T : EntityBehaviour
        {
            T newComponent = (T)Activator.CreateInstance<T>();

            this.OnDestroy += newComponent.OnEntityDestroyed;
            this.OnDisable += newComponent.OnEntityDisabled;
            this.OnEnable += newComponent.OnEntityEnabled;
            this.OnNotVisible += newComponent.OnEntityNotVisible;
            this.OnVisible += newComponent.OnEntityVisible;

            this.Components.Add(newComponent);

            return newComponent;
        }

        /// <summary>
        /// Remove the first component that match by type.
        /// </summary>
        /// <typeparam name="T">The type of the component to remove.</typeparam>
        public void RemoveComponent<T>() where T : EntityBehaviour
        {
            var component = this.Components.Where(e => e.GetType() == typeof(T)).FirstOrDefault();
            if (component != null && component != default(T))
            {
                component.Dispose();
                this.Components.Remove(component);
            }
        }

        /// <summary>
        /// Remove the component.
        /// </summary>
        /// <param name="component">Instance of the component.</param>
        public void RemoveComponent(EntityBehaviour component)
        {
            component.Dispose();
            this.Components.Remove(component);
        }

        /// <summary>
        /// Destroy this Entity instance.
        /// </summary>
        /// <param name="delay">Delay time for destroy the instance. Use 0 to destory inmediately.</param>
        public void Destroy(float delay = 0f)
        {
            Entity.Destroy(this, delay);
        }

        /// <summary>
        /// Search and get the first Entity that match by name in children hierarchy.
        /// </summary>
        /// <param name="name">Name of the Entity.</param>
        /// <returns>The first Entity that match.</returns>
        public Entity FindInChildrens(string name)
        {
            Entity ret = this.Childrens.Where(e => e.Name.Equals(name)).FirstOrDefault();

            if (ret == null)
            {
                foreach (var child in this.Childrens)
                {
                    ret = child.FindInChildrens(name);
                    if (ret != null) break;
                }
            }

            return ret;
        }

        /// <summary>
        /// Search and get the first Entity that match by tag in children hierarchy.
        /// </summary>
        /// <param name="tag">The tag of the Entity.</param>
        /// <returns>The first Entity that match.</returns>
        public Entity FindByTagInChildrens(string tag)
        {
            Entity ret = this.Childrens.Where(e => e.Tag.Equals(tag)).FirstOrDefault();

            if (ret == null)
            {
                foreach (var child in this.Childrens)
                {
                    ret = child.FindByTagInChildrens(tag);
                    if (ret != null) break;
                }
            }

            return ret;
        }

        /// <summary>
        /// Search and get the first Entity that match by type in children hierarchy.
        /// </summary>
        /// <typeparam name="T">Type of the component attached to the Entity.</typeparam>
        /// <returns>The first Entity that match.</returns>
        public Entity FindByTypeInChildrens<T>()
        {
            Entity ret = this.Childrens.Where(e => e.GetComponent<T>() != null).FirstOrDefault();

            if (ret == null)
            {
                foreach (var child in this.Childrens)
                {
                    ret = this.Childrens[i].FindByTypeInChildrens<T>();
                    if (ret != null) break;
                }
            }

            return ret;
        }

        /// <summary>
        /// Check if this Entity has the desired component.
        /// </summary>
        /// <typeparam name="T">Type of the component.</typeparam>
        /// <returns>Return true if the Entity has the component.</returns>
        public bool HasComponent<T>()
        {
            return this.GetComponent<T>() != null;
        }

        /// <summary>
        /// Search and get the first component that match by type.
        /// </summary>
        /// <typeparam name="T">Type of the component.</typeparam>
        /// <returns>Return the instance of the component.</returns>
        public T GetComponent<T>()
        {
            return (T)(object)this.Components.Where(e => e.GetType() == typeof(T)).FirstOrDefault();

            #region No Linq version
            //for (int i = 0; i < this.Components.Count; i++)
            //{
            //    if (this.Components[i].GetType() == typeof(T))
            //    {
            //        return (T)(object)this.Components[i];
            //    }
            //}
            //return (T)(object)null; 
            #endregion
        }

        /// <summary>
        /// Search all components that match by type.
        /// </summary>
        /// <typeparam name="T">Type of the component.</typeparam>
        /// <returns>Array with the all component instances.</returns>
        public T[] GetComponents<T>()
        {
            return (T[])(object)this.Components.Where(e => e.GetType() == typeof(T)).ToArray();
        }

        /// <summary>
        /// Search and get the first component that match by type in children Entities.
        /// </summary>
        /// <typeparam name="T">Type of the component.</typeparam>
        /// <returns>Return the instance of the component.</returns>
        public T GetComponentInChildrens<T>()
        {
            T ret = (T)(object)null;

            foreach (var child in this.Childrens)
            {
                ret = child.GetComponent<T>();
                if (ret != null) return ret;
            }

            foreach (var child in this.Childrens)
            {
                ret = child.GetComponentInChildrens<T>();
                if (ret != null) return ret;
            }

            return ret;
        }

        /// <summary>
        /// Search all components that match by type in children Entities.
        /// </summary>
        /// <typeparam name="T">Type of the component.</typeparam>
        /// <returns>Array with the all component instances.</returns>
        public T[] GetComponentsInChildrens<T>()
        {
            List<T> ret = new List<T>();

            foreach (var child in this.Childrens)
            {
                ret.AddRange(child.GetComponents<T>());
                ret.AddRange(child.GetComponentsInChildrens<T>());
            }

            return ret.ToArray();
        }

        /// <summary>
        /// Invokes a method, with delay or not, and interval for repeating calling.
        /// </summary>
        /// <param name="method">The method to invoke.</param>
        /// <param name="delay">Delay if want to invoke passed a desired time, in seconds. 0 for instant invoke.</param>
        /// <param name="interval">Interval time, in seconds, for repeating invokes. 0 for one invoke.</param>
        public void Invoke(System.Timers.ElapsedEventHandler method, float delay = 0f, float interval = 0f)
        {
            this.InvokeManager.Invoke(method, delay, interval);
        }

        /// <summary>
        /// Cancel all invokes in this Entity.
        /// </summary>
        public void CancelAllInvokes()
        {
            this.InvokeManager.CancelAllInvokes();
        }

        /// <summary>
        /// Cancel the invoke that match by method.
        /// </summary>
        /// <param name="method">The method invoked to cancel.</param>
        public void CancelInvoke(System.Timers.ElapsedEventHandler method)
        {
            this.InvokeManager.CancelInvoke(method);
        }

        internal void Update()
        {
            if (this.Enable)
            {
                // Update delay invokes:
                this.InvokeManager.Update();

                // Update components:
                this.Components.Update();

                // Update all enabled childrens:
                foreach (var child in this.Childrens)
                {
                    child.Update();
                }
            }
        }

        internal void Draw()
        {
            // Render components:
            if (this.Visible)
            {
                this.Components.Draw();

                // Draw all visibled childrens:
                foreach (var child in this.Childrens)
                {
                    child.Draw();
                }
            }
        }
        #endregion
        #endregion
    }
}
