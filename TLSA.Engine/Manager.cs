﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TLSA.Engine.Core;
using TLSA.Engine.Core.Graphics;
using TLSA.Engine.Core.Audio;
using TLSA.Engine.Core.Input;
using TLSA.Engine.Core.Video;

namespace TLSA.Engine
{
    public class Manager
    {
		#region Singleton
		private static Manager _instance;

		public static Manager Instance
		{
			get
			{
				return _instance != null ? _instance : null;
			}
		}

		/// <summary>
		/// Initialize engine instance.
		/// </summary>
		/// <param name="hWnd">Windows handle.</param>
		/// <param name="width">Graphic render width resolution.</param>
		/// <param name="height">Graphic render height resolution.</param>
		public static void Initialize(int hWnd, int width, int height)
		{
			Manager._instance = new Manager();

			Manager.Instance.Components = new ComponentPool();
			Manager.Instance._frameTimer = new Timer();
			Manager.Instance.DeltaTime = 0f;

			GraphicsManager.Instance.Initialize(hWnd, width, height);
			InputManager.Instance.Initialize(hWnd);
			AudioManager.Instance.Initialize(hWnd);
			VideoManager.Instance.Initialize(hWnd);
		}

		/// <summary>
		/// Terminate engine instance.
		/// </summary>
		public static void Terminate()
		{
			Manager.Instance.Components.Dispose();
			Manager.Instance.Graphics.Dispose();
			Manager.Instance.Input.Dispose();
			Manager.Instance.Audio.Dispose();
			Manager.Instance.Video.Dispose();

			Manager._instance = null;
		}
		#endregion

		#region Internal vars
		private Timer _frameTimer; 
        #endregion

        #region Properties
		/// <summary>
		/// Base component pool.
		/// </summary>
        public ComponentPool Components { get; private set; }

		/// <summary>
		/// Access to graphics manager instance.
		/// </summary>
        public GraphicsManager Graphics { get { return GraphicsManager.Instance; } }

		/// <summary>
		/// Access to input manager instance.
		/// </summary>
        public InputManager Input { get { return InputManager.Instance; } }

		/// <summary>
		/// Access to audio manager instance.
		/// </summary>
        public AudioManager Audio { get { return AudioManager.Instance; } }

		/// <summary>
		/// Acccess to video manager instance.
		/// </summary>
        public VideoManager Video { get { return VideoManager.Instance; } }

		/// <summary>
		/// Duration in seconds of the last frame.
		/// </summary>
        public float DeltaTime { get; private set; }

		/// <summary>
		/// Frames per second rate of the last frame.
		/// </summary>
        public int FramesPerSecond { get { return Graphics.FPS; } }
        #endregion

        #region Methods & functions
		/// <summary>
		/// Update logic engine.
		/// </summary>
        public void Update()
        {
            this._frameTimer.Reset(); // Reset frame timer.

            this.Input.Update(); // Update player input.			
            this.Components.Update(); // Update base components.
		}

		/// <summary>
		/// Render current frame.
		/// </summary>
        public void Draw()
        {
            this.Graphics.Begin();
			{
				this.Components.Draw(); // Render base components.
			}
            this.Graphics.End();
            
            this.DeltaTime = this._frameTimer.Time; // Get the frame duration.
        } 
        #endregion
    }
}
