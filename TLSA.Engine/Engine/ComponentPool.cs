﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace TLSA.Engine
{
    /// <summary>
    /// Component pool. Used by GameObjects to attach components.
    /// </summary>
    public class ComponentPool : IDisposable, IEnumerable<ComponentBase>
    {
        #region Internal vars
        internal GameObject _owner;
        List<ComponentBase> _components;
        internal bool _hasChanges;
        #endregion

        #region Properties
        public ComponentBase this[int index]
        {
            get { return this._components[index]; }
            set
            {
                this._components[index] = value;
                this._hasChanges = true;
            }
        }

        public int Count { get { return this._components.Count; } }
        #endregion

        #region Constructors
        internal ComponentPool()
        {
            this._components = new List<ComponentBase>();
            this.Clear();
        }
        #endregion

        #region Destructors
        public void Dispose()
        {
            this.Clear();
        }
        #endregion

        #region Methods & functions
        internal void Add(ComponentBase component)
        {
            this._components.Add(component);
            component._owner = this;
            this._hasChanges = true;
        }

        internal void Remove(ComponentBase component)
        {
            component.Dispose();
            this._components.Remove(component);
            this._hasChanges = true;
        }

        internal void Clear()
        {
            foreach (var component in this._components)
            {
                component.Dispose();
            }

            this._components.Clear();
        }

        internal void Update()
        {
            if (this._hasChanges) this._components = this._components.OrderByDescending(e => e.Priority).ToList();

            this._hasChanges = false;

            foreach (var component in this._components)
            {
                if (component.Enable)
                {
                    component.Update();
                }
            }
        }

        internal void Draw()
        {
            foreach (var component in this._components)
            {
                if (component.Enable)
                {
                    component.Draw();
                }
            }
        }

        public IEnumerator<ComponentBase> GetEnumerator()
        {
            return ((IEnumerable<ComponentBase>)this._components).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<ComponentBase>)this._components).GetEnumerator();
        }
        #endregion
    }
}
