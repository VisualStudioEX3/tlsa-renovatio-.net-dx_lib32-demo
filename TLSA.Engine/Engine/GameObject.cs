﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TLSA.Engine.Core;
using TLSA.Engine.Core.Graphics;

namespace TLSA.Engine
{
    /// <summary>
    /// Game Object. Base class for all entities in the scene.
    /// </summary>
    public class GameObject
    {
        #region Static class
        #region Methods & functions
        /// <summary>
        /// Find a GameObject in the scene by name.
        /// </summary>
        /// <param name="name">Name of the GameObject.</param>
        /// <returns>Return the first GameObject that match by name.</returns>
        public static GameObject Find(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Find a GameObject in the scene by tag.
        /// </summary>
        /// <param name="name">Tag of the GameObject.</param>
        /// <returns>Return the first GameObject that match by tag.</returns>
        public static GameObject FindByTag(string tag)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Find a GameObject in the scene by type.
        /// </summary>
        /// <param name="name">Type of the component attached to GameObject.</param>
        /// <returns>Return the first GameObject that has component that match by type.</returns>
        public static GameObject FindByType<T>()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Destroy a GameObject instance.
        /// </summary>
        /// <param name="instance">Instance of the GameObject.</param>
        /// <param name="delay">Delay time for destroy the instance. Use 0 to destory inmediately.</param>
        public static void Destroy(GameObject instance, float delay = 0f)
        {
            // Maybe try implement a pool system.

            // Call event:
            if (instance.OnDestroy != null)
            {
                instance.OnDestroy();
            }
        }
        #endregion
        #endregion

        #region Instance class
        #region Internal vars
        bool _enable;
        bool _visible;
        internal InvokeManager InvokeManager;
        #endregion

        #region Properties
        /// <summary>
        /// Unique id for this GameObject.
        /// </summary>
        public Guid Id { get; private set; }

        /// <summary>
        /// Name used by this instance.
        /// </summary>
        /// <remarks>This value no is the unique id of the instance.</remarks>
        public string Name { get; set; }

        /// <summary>
        /// Enable or disabled this instance.
        /// </summary>
        /// <remarks>This affect to invoked methods with delay to start.</remarks>
        public bool Enable
        {
            get
            {
                return this._enable;
            }
            set
            {
                this._enable = this.InvokeManager.Enabled = value; 
                if (this._enable)
                {
                    if (this.OnEnable != null)
                    {
                        this.OnEnable();
                    }
                }
                else
                {
                    if (this.OnDisable != null)
                    {
                        this.OnDisable();
                    }
                }
            }
        }

        /// <summary>
        /// Enable or disabled this instance.
        /// </summary>
        public bool Visible
        {
            get
            {
                return this._visible;
            }
            set
            {
                this._visible = value;
                if (this._visible)
                {
                    if (this.OnVisible != null)
                    {
                        this.OnVisible();
                    }
                }
                else
                {
                    if (this.OnNotVisible != null)
                    {
                        this.OnNotVisible();
                    }
                }
            }
        }

        /// <summary>
        /// Components attached to this GameObject.
        /// </summary>
        public ComponentPool Components { get; private set; }

        /// <summary>
        /// Execution order priority.
        /// </summary>
        public int Priority { get; set; }
        
        /// <summary>
        /// Father GameObject. Null if not have.
        /// </summary>
        public GameObject Parent { get; set; }
        
        /// <summary>
        /// Child GameObjects.
        /// </summary>
        public List<GameObject> Childrens { get; private set; }
        
        /// <summary>
        /// GameObject Tag.
        /// </summary>
        public string Tag { get; set; }
        
        /// <summary>
        /// GameObject Layer.
        /// </summary>
        public int Layer { get; set; }

        /// <summary>
        /// Position of GameObject. (Probably change this property in the future with a Transform object)
        /// </summary>
        public Point2D Position { get; private set; }

        /// <summary>
        /// Sprite attached to this GameObject (need testing)
        /// </summary>
        public Sprite Sprite { get; private set; }
        #endregion

        #region Delegates
        public delegate void OnDestroyGameObjectHandler();
        public delegate void OnEnableGameObjectHandler();
        public delegate void OnDisableGameObjectHandler();
        public delegate void OnVisibleGameObjectHandler();
        public delegate void OnNotVisibleGameObjectHandler();

        /// <summary>
        /// Event raised when this GameObject is destroyed.
        /// </summary>
        public OnDestroyGameObjectHandler OnDestroy;

        /// <summary>
        /// Event raised when this GameObject is enabled.
        /// </summary>
        public OnEnableGameObjectHandler OnEnable;

        /// <summary>
        /// Event raised when this GameObject is disabled.
        /// </summary>
        public OnDisableGameObjectHandler OnDisable;

        /// <summary>
        /// Event raised when this GameObject is set visible.
        /// </summary>
        public OnVisibleGameObjectHandler OnVisible;

        /// <summary>
        /// Event raised when this GameObject is set not visible.
        /// </summary>
        public OnNotVisibleGameObjectHandler OnNotVisible;
        #endregion

        #region Constructor
        public GameObject()
        {
            this.Id = Guid.NewGuid();

            this.InvokeManager = new InvokeManager(this);
            this.Components = new ComponentPool();
            this.Childrens = new List<GameObject>();
        }
        #endregion

        #region Methods & functions
        /// <summary>
        /// Add a component to the GameObject.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>Return the new component instance.</returns>
        public T AddComponent<T>() where T : TLSABehaviour
        {
            T newComponent = (T)Activator.CreateInstance<T>();
            
            this.OnDestroy += newComponent.OnGameObjectDestroyed;
            this.OnDisable += newComponent.OnGameObjectDisabled;
            this.OnEnable += newComponent.OnGameObjectEnabled;
            this.OnNotVisible += newComponent.OnGameObjectNotVisible;
            this.OnVisible += newComponent.OnGameObjectVisible;

            this.Components.Add(newComponent);

            return newComponent;
        }

        /// <summary>
        /// Remove the first component that match by type.
        /// </summary>
        /// <typeparam name="T">The type of the component to remove.</typeparam>
        public void RemoveComponent<T>() where T : TLSABehaviour
        {
            var component = this.Components.Where(e => e.GetType() == typeof(T)).FirstOrDefault();
            if (component != null && component != default(T))
            {
                component.Dispose();
                this.Components.Remove(component); 
            }
        }

        /// <summary>
        /// Remove the component.
        /// </summary>
        /// <param name="component">Instance of the component.</param>
        public void RemoveComponent(TLSABehaviour component)
        {
            component.Dispose();
            this.Components.Remove(component);
        }

        /// <summary>
        /// Destroy this GameObject instance.
        /// </summary>
        /// <param name="delay">Delay time for destroy the instance. Use 0 to destory inmediately.</param>
        public void Destroy(float delay = 0f)
        {
            GameObject.Destroy(this, delay);
        }

        /// <summary>
        /// Search and get the first GameObject that match by name in children hierarchy.
        /// </summary>
        /// <param name="name">Name of the GameObject.</param>
        /// <returns>The first GameObject that match.</returns>
        public GameObject FindInChildrens(string name)
        {
            GameObject ret = null;

            // Search in top level list:
            for (int i = 0; i < this.Childrens.Count; i++)
            {
                if (this.Childrens[i].Name == name)
                {
                    ret = this.Childrens[i];
                    break;
                }
            }

            if (ret == null)
            {
                // Search in each element:
                for (int i = 0; i < this.Childrens.Count; i++)
                {
                    ret = this.Childrens[i].FindInChildrens(name);
                    if (ret != null) break;
                }
            }

            return ret;
        }

        /// <summary>
        /// Search and get the first GameObject that match by tag in children hierarchy.
        /// </summary>
        /// <param name="tag">The tag of the GameObject.</param>
        /// <returns>The first GameObject that match.</returns>
        public GameObject FindByTagInChildrens(string tag)
        {
            GameObject ret = null;

            // Search in top level list:
            for (int i = 0; i < this.Childrens.Count; i++)
            {
                if (this.Childrens[i].Tag == tag)
                {
                    ret = this.Childrens[i];
                    break;
                }
            }

            if (ret == null)
            {
                // Search in each element:
                for (int i = 0; i < this.Childrens.Count; i++)
                {
                    ret = this.Childrens[i].FindByTagInChildrens(tag);
                    if (ret != null) break;
                }
            }

            return ret;
        }

        /// <summary>
        /// Search and get the first GameObject that match by type in children hierarchy.
        /// </summary>
        /// <typeparam name="T">Type of the component attached to the GameObject.</typeparam>
        /// <returns>The first GameObject that match.</returns>
        public GameObject FindByTypeInChildrens<T>()
        {
            GameObject ret = null;

            // Search in top level list:
            for (int i = 0; i < this.Childrens.Count; i++)
            {
                if (this.Childrens[i].HasComponent<T>())
                {
                    ret = this.Childrens[i];
                    break;
                }
            }

            if (ret == null)
            {
                // Search in each element:
                for (int i = 0; i < this.Childrens.Count; i++)
                {
                    ret = this.Childrens[i].FindByTypeInChildrens<T>();
                    if (ret != null) break;
                }
            }

            return ret;
        }

        /// <summary>
        /// Check if this GameObject has the desired component.
        /// </summary>
        /// <typeparam name="T">Type of the component.</typeparam>
        /// <returns>Return true if the GameObject has the component.</returns>
        public bool HasComponent<T>()
        {
            for (int i = 0; i < this.Components.Count; i++)
            {
                if (this.Components[i].GetType() == typeof(T))
                {
                    return true;
                }            
            }
            return false;
        }

        /// <summary>
        /// Search and get the first component that match by type.
        /// </summary>
        /// <typeparam name="T">Type of the component.</typeparam>
        /// <returns>Return the instance of the component.</returns>
        public T GetComponent<T>()
        {
            for (int i = 0; i < this.Components.Count; i++)
            {
                if (this.Components[i].GetType() == typeof(T))
                {
                    return (T)(object)this.Components[i];
                }
            }
            return (T)(object)null;
        }

        /// <summary>
        /// Search all components that match by type.
        /// </summary>
        /// <typeparam name="T">Type of the component.</typeparam>
        /// <returns>Array with the all component instances.</returns>
        public T[] GetComponents<T>()
        {
            return (T[])(object)this.Components.Where(e => e.GetType() == typeof(T)).ToArray();
        }

        /// <summary>
        /// Search and get the first component that match by type in children GameObjects.
        /// </summary>
        /// <typeparam name="T">Type of the component.</typeparam>
        /// <returns>Return the instance of the component.</returns>
        public T GetComponentInChildrens<T>()
        {
            T ret;

            // Search in top level list:
            for (int i = 0; i < this.Childrens.Count; i++)
            {
                ret = this.Childrens[i].GetComponent<T>();
                if (ret != null)
                {
                    return ret;
                }
            }

            // Search in each element:
            for (int i = 0; i < this.Childrens.Count; i++)
            {
                ret = this.Childrens[i].GetComponentInChildrens<T>();
                if (ret != null)
                {
                    return ret;
                }
            }

            return (T)(object)null;
        }

        /// <summary>
        /// Search all components that match by type in children GameObjects.
        /// </summary>
        /// <typeparam name="T">Type of the component.</typeparam>
        /// <returns>Array with the all component instances.</returns>
        public T[] GetComponentsInChildrens<T>()
        {
            List<T> ret = new List<T>();

            // Search in top level list:
            for (int i = 0; i < this.Childrens.Count; i++)
            {
                if (this.Childrens[i].GetType() == typeof(T))
                {
                    ret.Add((T)(object)this.Childrens[i]);
                }

                // Search in each item:
                ret.AddRange(this.Childrens[i].GetComponentsInChildrens<T>());
            }

            return ret.ToArray();
        }

        /// <summary>
        /// Invokes a method, with delay or not, and interval for repeating calling.
        /// </summary>
        /// <param name="method">The method to invoke.</param>
        /// <param name="delay">Delay if want to invoke passed a desired time, in seconds. 0 for instant invoke.</param>
        /// <param name="interval">Interval time, in seconds, for repeating invokes. 0 for one invoke.</param>
        public void Invoke(System.Timers.ElapsedEventHandler method, float delay = 0f, float interval = 0f)
        {
            this.InvokeManager.Invoke(method, delay, interval);
        }

        /// <summary>
        /// Cancel all invokes in this GameObject.
        /// </summary>
        public void CancelInvoke()
        {
            this.InvokeManager.CancelInvoke();
        }

        /// <summary>
        /// Cancel the invoke that match by method.
        /// </summary>
        /// <param name="method">The method invoked to cancel.</param>
        public void CancelInvoke(System.Timers.ElapsedEventHandler method)
        {
            this.InvokeManager.CancelInvoke(method);
        }

        /// <summary>
        /// This method is called by the engine.
        /// </summary>
        void InternalUpdate()
        {
            if (this.Enable)
            {
                // Update delay invokes:
                this.InvokeManager.Update();

                // Update components:
                this.Components.Update();
            }

            // Render components:
            if (this.Visible)
            {
                this.Components.Draw();
            }
        }
        #endregion
        #endregion
    }
}
