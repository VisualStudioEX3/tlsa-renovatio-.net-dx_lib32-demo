﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine
{
    /// <summary>
    /// The base class to implement own component behaviours for GameObjects.
    /// </summary>
    public abstract class TLSABehaviour : ComponentBase
    {
        // TODO: Implement access to main-by default components like Transform and Sprite.

        #region Methods & functions
        /// <summary>
        /// Shortcut to Debug.Log() function.
        /// </summary>
        /// <param name="message">Message to log.</param>
        public void Print(string message)
        {
            Debug.Log(message);
        }

        /// <summary>
        /// Destroy the GameObject instance where this component is attached.
        /// </summary>
        /// <param name="delay">Delay time for destroy the instance. Use 0 to destory inmediately.</param>
        public void Destroy(float delay = 0)
        {
            this.GameObject.Destroy(delay);
        }

        /// <summary>
        /// Add a component to the GameObject where this component is attached.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>Return the new component instance.</returns>
        public T AddComponent<T>() where T : TLSABehaviour
        {
            return this.GameObject.AddComponent<T>();
        }

        /// <summary>
        /// Remove the first component that match by type on the GameObject where this component is attached.
        /// </summary>
        /// <typeparam name="T">The type of the component to remove.</typeparam>
        public void RemoveComponent<T>() where T : TLSABehaviour
        {
            this.GameObject.RemoveComponent<T>();
        }

        /// <summary>
        /// Remove the component on the GameObject where this component is attached.
        /// </summary>
        /// <param name="component">Instance of the component.</param>
        public void RemoveComponent(TLSABehaviour component)
        {
            this.GameObject.RemoveComponent(component);
        }

        /// <summary>
        /// Search and get the first GameObject that match by name in children hierarchy.
        /// </summary>
        /// <param name="name">Name of the GameObject.</param>
        /// <returns>The first GameObject that match.</returns>
        public GameObject FindInChildrens(string name)
        {
            return this.GameObject.FindInChildrens(name);
        }

        /// <summary>
        /// Search and get the first GameObject that match by tag in children hierarchy.
        /// </summary>
        /// <param name="tag">The tag of the GameObject.</param>
        /// <returns>The first GameObject that match.</returns>
        public GameObject FindByTagInChildrens(string tag)
        {
            return this.GameObject.FindByTagInChildrens(tag);
        }

        /// <summary>
        /// Search and get the first GameObject that match by type in children hierarchy.
        /// </summary>
        /// <typeparam name="T">Type of the component attached to the GameObject.</typeparam>
        /// <returns>The first GameObject that match.</returns>
        public GameObject FindByTypeInChildrens<T>() where T : TLSABehaviour
        {
            return this.GameObject.FindByTypeInChildrens<T>();
        }

        /// <summary>
        /// Check if this GameObject has the desired component.
        /// </summary>
        /// <typeparam name="T">Type of the component.</typeparam>
        /// <returns>Return true if the GameObject has the component.</returns>
        public bool HasComponent<T>() where T : TLSABehaviour
        {
            return this.GameObject.HasComponent<T>();
        }

        /// <summary>
        /// Search and get the first component that match by type.
        /// </summary>
        /// <typeparam name="T">Type of the component.</typeparam>
        /// <returns>Return the instance of the component.</returns>
        public T GetComponent<T>() where T : TLSABehaviour
        {
            return this.GameObject.GetComponent<T>();
        }

        /// <summary>
        /// Search all components that match by type.
        /// </summary>
        /// <typeparam name="T">Type of the component.</typeparam>
        /// <returns>Array with the all component instances.</returns>
        public T[] GetComponents<T>() where T : TLSABehaviour
        {
            return this.GameObject.GetComponents<T>();
        }

        /// <summary>
        /// Search and get the first component that match by type in children GameObjects.
        /// </summary>
        /// <typeparam name="T">Type of the component.</typeparam>
        /// <returns>Return the instance of the component.</returns>
        public T GetComponentInChildrens<T>() where T : TLSABehaviour
        {
            return this.GameObject.GetComponentInChildrens<T>();
        }

        /// <summary>
        /// Search all components that match by type in children GameObjects.
        /// </summary>
        /// <typeparam name="T">Type of the component.</typeparam>
        /// <returns>Array with the all component instances.</returns>
        public T[] GetComponentsInChildrens<T>() where T : TLSABehaviour
        {
            return this.GameObject.GetComponentsInChildrens<T>();
        }

        /// <summary>
        /// Invokes a method, with delay or not, and interval for repeating calling.
        /// </summary>
        /// <param name="method">The method to invoke.</param>
        /// <param name="delay">Delay if want to invoke passed a desired time, in seconds. 0 for instant invoke.</param>
        /// <param name="interval">Interval time, in seconds, for repeating invokes. 0 for one invoke.</param>
        public void Invoke(System.Timers.ElapsedEventHandler method, float delay = 0f, float interval = 0f)
        {
            this.GameObject.InvokeManager.Invoke(method, delay, interval);
        }

        /// <summary>
        /// Cancel all invokes in this GameObject.
        /// </summary>
        public void CancelInvoke()
        {
            this.GameObject.InvokeManager.CancelInvoke();
        }

        /// <summary>
        /// Cancel the invoke that match by method.
        /// </summary>
        /// <param name="method">The method invoked to cancel.</param>
        public void CancelInvoke(System.Timers.ElapsedEventHandler method)
        {
            this.GameObject.InvokeManager.CancelInvoke(method);
        } 
        #endregion
    }
}
