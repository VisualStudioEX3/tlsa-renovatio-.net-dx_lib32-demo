﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TLSA.Engine.Core;

namespace TLSA.Engine
{
    /// <summary>
    /// Manager for invoke logic in Entities.
    /// </summary>
    internal class InvokeManager : IDisposable
    {
        #region Internal struct
        private struct InvokeData
        {
            public System.Timers.Timer Event;
            public System.Timers.ElapsedEventHandler Method;
            public float Delay;
            public Timer DelayTimer;
            public bool HasStarted;
        }
        #endregion

        #region Internal vars
        bool _enabled;
        List<InvokeData> _invokes;
        internal Entity _owner;
        #endregion

        #region Properties
        public bool Enabled
        {
            get
            {
                return this._enabled;
            }
            set
            {
                this._enabled = value;
                for (int i = 0; i < this._invokes.Count; i++)
                {
                    var invoke = this._invokes[i];
                    if (invoke.DelayTimer != null)
                    {
                        invoke.DelayTimer.IsPaused = value;
                    }
                    if (invoke.HasStarted)
                    {
                        invoke.Event.Enabled = value;
                    }
                }
            }
        }
        #endregion

        #region Constructor & destructor
        internal InvokeManager(Entity owner)
        {
            this._owner = owner;
            this._invokes = new List<InvokeData>();
            this._enabled = true;
        }

        public void Dispose()
        {
            this.CancelAllInvokes();
        } 
        #endregion

        #region Methods & functions
        public void Update()
        {
            for (int i = 0; i < this._invokes.Count; i++)
            {
                var invoke = this._invokes[i];
                if (invoke.Delay > 0f && !invoke.Event.Enabled)
                {
                    if (invoke.DelayTimer.Time >= invoke.Delay)
                    {
                        invoke.Event.Start();
                        invoke.HasStarted = true;
                    }
                }
            }
        }

        public void Invoke(System.Timers.ElapsedEventHandler method, float delay = 0f, float interval = 0f)
        {
            var invoke = new InvokeData();
            invoke.Method = method;
            invoke.Event.Interval = interval;
            invoke.Event.AutoReset = interval > 0;
            invoke.Event.Elapsed += method;
            invoke.Event.Enabled = delay == 0;
            invoke.Delay = delay;
            invoke.DelayTimer = delay > 0 ? new Timer() : null;
            this._invokes.Add(invoke);
        }

        public void CancelAllInvokes()
        {
            for (int i = 0; i < this._invokes.Count; i++)
            {
                this._invokes[i].Event.Stop();
                this._invokes[i].Event.Dispose();
            }
            this._invokes.Clear();
        }

        public void CancelInvoke(System.Timers.ElapsedEventHandler method)
        {
            var invoke = this._invokes.Where(e => e.Method.Equals(method)).FirstOrDefault();
            if (invoke.Method != null)
            {
                invoke.Event.Stop();
                invoke.Event.Dispose();
                this._invokes.Remove(invoke);
            }
        }
        #endregion
    }
}
