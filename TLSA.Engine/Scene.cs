﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TLSA.Engine.Core;

namespace TLSA.Engine
{
    /// <summary>
    /// Contains all entities in scene.
    /// </summary>
    public struct Scene
    {
        #region Public vars
        /// <summary>
        /// Name of the scene.
        /// </summary>
        public string Name;

        /// <summary>
        /// List of the root entities.
        /// </summary>
        public List<Entity> Entities;
        #endregion

        #region Constructors
        internal Scene(string name)
        {
            this.Name = name;
            this.Entities = new List<Entity>();
        }
        #endregion

        #region Methods & Functions
        /// <summary>
        /// Search and get the first Entity that match by name in scene hierarchy.
        /// </summary>
        /// <param name="name">The name of the Entity.</param>
        /// <returns>The first Entity that match.</returns>
        public Entity Find(string name)
        {
            var ret = this.Entities.Where(e => e.Name.Equals(name)).FirstOrDefault();

            if (ret == null)
            {
                foreach (var entity in this.Entities)
                {
                    ret = entity.FindInChildrens(name);
                    if (ret != null) break;
                } 
            }

            return ret;
        }

        /// <summary>
        /// Search and get the first Entity that match by tag in scene hierarchy.
        /// </summary>
        /// <param name="tag">The tag of the Entity.</param>
        /// <returns>The first Entity that match.</returns>
        public Entity FindByTag(string tag)
        {
            var ret = this.Entities.Where(e => e.Tag.Equals(tag)).FirstOrDefault();

            if (ret == null)
            {
                foreach (var entity in this.Entities)
                {
                    ret = entity.FindByTagInChildrens(tag);
                    if (ret != null) break;
                }
            }

            return ret;
        }

        /// <summary>
        /// Search and get the first Entity that match by type in children hierarchy.
        /// </summary>
        /// <typeparam name="T">Type of the component attached to the Entity.</typeparam>
        /// <returns>The first Entity that match.</returns>
        public Entity FindByType<T>() where T : EntityBehaviour
        {
            Entity ret = this.Entities.Where(e => e.GetComponent<T>() != null).FirstOrDefault();

            if (ret == null)
            {
                foreach (var entity in this.Entities)
                {
                    ret = entity.FindByTypeInChildrens<T>();
                    if (ret != null) break;
                }
            }

            return ret;
        }

        internal void Update()
        {
            foreach (var entity in this.Entities)
            {
                entity.Update();
            }
        }

        internal void Draw()
        {
            foreach (var entity in this.Entities)
            {
                entity.Draw();
            }
        } 
        #endregion
    }
}
