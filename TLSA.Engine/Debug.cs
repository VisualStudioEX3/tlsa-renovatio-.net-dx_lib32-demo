﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine
{
    /// <summary>
    /// Debug helper. Shortcut to System.Diagnostics.Debug object.
    /// </summary>
    public static class Debug
    {
        #region Methods & Functions
        public static void Log(string message)
        {
            System.Diagnostics.Debug.Print(message);
        }

        public static void Log(string format, params object[] args)
        {
            System.Diagnostics.Debug.Print(format, args);
        }

        public static void Assert(bool condition)
        {
            System.Diagnostics.Debug.Assert(condition);
        }

        public static void Assert(bool condition, string message)
        {
            System.Diagnostics.Debug.Assert(condition, message);
        }

        public static void Assert(bool condition, string message, string detailMessage)
        {
            System.Diagnostics.Debug.Assert(condition, message, detailMessage);
        }

        public static void Assert(bool condition, string message, string detailMessage, params object[] args)
        {
            System.Diagnostics.Debug.Assert(condition, message, detailMessage, args);
        }

        public static void Fail(string message)
        {
            System.Diagnostics.Debug.Fail(message);
        }

        public static void Fail(string message, string detailMessage)
        {
            System.Diagnostics.Debug.Fail(message, detailMessage);
        } 
        #endregion
    }
}
