﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine
{
    /// <summary>
    /// Base code for implementing engine components.
    /// </summary>
    public abstract class ComponentBase : IDisposable
    {
        #region Internal vars
        bool _enabled = true;
        internal ComponentPool _owner;
        private int _priority = 0;
        #endregion

        #region Properties
        /// <summary>
        /// Entity where this component is attached.
        /// </summary>
        public Entity Entity { get { return _owner._owner; } }

        /// <summary>
        /// Execution order priority.
        /// </summary>
        public int Priority { get { return _priority; } set { _priority = value; _owner._hasChanges = true; } }

        /// <summary>
        /// Enable or disabled this component.
        /// </summary>
        public bool Enable
        {
            get
            {
                return this._enabled;
            }
            set
            {
                this._enabled = value;
                if (value)
                {
                    this.OnEnabled();
                }
                else
                {
                    this.OnDisabled();
                }
            }
        }
        #endregion

        #region Methods & functions
        /// <summary>
        /// Reset values.
        /// </summary>
        public virtual void Reset()
        {

        }

        /// <summary>
        /// Is calling when the component is loaded.
        /// </summary>
        public virtual void Awake()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void Start()
        {

        }

        /// <summary>
        /// Is calling when the component is enabled.
        /// </summary>
        public virtual void OnEnabled()
        {

        }

        /// <summary>
        /// Is calling when the component is diabled.
        /// </summary>
        public virtual void OnDisabled()
        {

        }

        /// <summary>
        /// Is calling when the Entity is set visible.
        /// </summary>
        public virtual void OnEntityVisible()
        {

        }

        /// <summary>
        /// Is calling when the Entity is set not visible.
        /// </summary>
        public virtual void OnEntityNotVisible()
        {

        }

        /// <summary>
        /// Is calling when the Entity is destroyed.
        /// </summary>
        public virtual void OnEntityDestroyed()
        {

        }

        /// <summary>
        /// Is calling when the Entity is enabled.
        /// </summary>
        public virtual void OnEntityEnabled()
        {

        }

        /// <summary>
        /// Is calling when the Entity is diabled.
        /// </summary>
        public virtual void OnEntityDisabled()
        {

        }

        /// <summary>
        /// Update logic code.
        /// </summary>
        public virtual void Update()
        {

        }

        /// <summary>
        /// Draw logic code.
        /// </summary>
        public virtual void Draw()
        {

        }
        
        /// <summary>
        /// Its calling when the component instance is destroyed.
        /// </summary>
        public virtual void Dispose()
        {
            
        }
        #endregion
    }
}
