﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core
{
    /// <summary>
    /// Math helper class.
    /// </summary>
    public static class MathHelper
    {
        /// <summary>
        /// PI value.
        /// </summary>
        public const float PI = (float)Math.PI;

        /// <summary>
        /// Transform radians to degrees.
        /// </summary>
        /// <param name="angle">Angle in radians.</param>
        /// <returns>Return the angle in degrees.</returns>
        public static float ToDegrees(float angle)
        {
            return angle / PI * 180;
        }

        /// <summary>
        /// Transform the degree to radians.
        /// </summary>
        /// <param name="angle">Angle in degrees.</param>
        /// <returns>Return the angle in radians.</returns>
        public static float ToRadians(float angle)
        {
            return angle * PI / 180;
        }

        /// <summary>
        /// Linear interpolation between two values.
        /// </summary>
        /// <param name="a">First value.</param>
        /// <param name="b">Second value.</param>
        /// <param name="factor">Interpolation factor.</param>
        /// <returns></returns>
        public static float Lerp(float a, float b, float factor)
        {
            return a * factor + b * (1 - factor);
        }

        /// <summary>
        /// Clam a value between a maximun and minmun range.
        /// </summary>
        /// <param name="val">Value to clamp.</param>
        /// <param name="min">Minimal value range.</param>
        /// <param name="max">Maximal value range.</param>
        /// <returns>Return the clamp value.</returns>
        public static int Clamp(int val, int min, int max)
        {
            if (val.CompareTo(min) < 0) return min;
            else if (val.CompareTo(max) > 0) return max;
            else return val;
        }

        /// <summary>
        /// Clam a value between a maximun and minmun range.
        /// </summary>
        /// <param name="val">Value to clamp.</param>
        /// <param name="min">Minimal value range.</param>
        /// <param name="max">Maximal value range.</param>
        /// <returns>Return the clamp value.</returns>
        public static float Clamp(float val, float min, float max)
        {
            if (val.CompareTo(min) < 0) return min;
            else if (val.CompareTo(max) > 0) return max;
            else return val;
        }

        /// <summary>
        /// Determine if the float value is near to other float value.
        /// </summary>
        /// <param name="a">First value.</param>
        /// <param name="b">Second value.</param>
        /// <returns>Returns if the first value is near or the same as second value.</returns>
        public static bool Approximately(float a, float b)
        {
            return System.Math.Abs(a - b) <= float.Epsilon;
        }

        /// <summary>
        /// Calculate the percent from a value and range.
        /// </summary>
        /// <param name="value">Value to percent.</param>
        /// <param name="range">Range max value (e.g. 100)</param>
        /// <returns>Return the percent value.</returns>
        public static float PercentFromValue(int value, int range)
        {
            return value / range * 100;
        }

        /// <summary>
        /// Calculate the percent from a value and range.
        /// </summary>
        /// <param name="value">Value to percent.</param>
        /// <param name="range">Range max value (e.g. 100)</param>
        /// <returns>Return the percent value.</returns>
        public static float PercentFromValue(float value, float range)
        {
            return value / range * 100;
        }

        /// <summary>
        /// Calculate the value from a percent and range.
        /// </summary>
        /// <param name="percent">Percent value.</param>
        /// <param name="range">Range max value (e.g. 100)</param>
        /// <returns>Return the natural value.</returns>
        public static int ValueFromPercent(int percent, int range)
        {
            return percent * range / 100;
        }

        /// <summary>
        /// Calculate the value from a percent and range.
        /// </summary>
        /// <param name="percent">Percent value.</param>
        /// <param name="range">Range max value (e.g. 100)</param>
        /// <returns>Return the natural value.</returns>
        public static float ValueFromPercent(float percent, float range)
        {
            return percent * range / 100;
        }
    }
}
