﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using dxlib32_222;

namespace TLSA.Engine.Core
{
    /// <summary>
    /// Helper class.
    /// </summary>
    public static class Helper
    {
        internal static dx_System_Class dx_system = new dx_System_Class();

        /// <summary>
        /// Swap variable values.
        /// </summary>
        /// <typeparam name="T">Type of variables.</typeparam>
        /// <param name="a">First var.</param>
        /// <param name="b">Second var.</param>
        public static void Swap<T>(ref T a, ref T b)
        {
            T c = a; a = b; b = c;
        }

        /// <summary>
        /// Fill an array with a same value.
        /// </summary>
        /// <typeparam name="T">Array type data.</typeparam>
        /// <param name="array">Initialized array to fill.</param>
        /// <param name="value">Value to fill each element of array.</param>
        public static void Fill<T>(ref T[] array, T value)
        {
            array = Enumerable.Repeat<T>(value, array.Length).ToArray();
        }

        /// <summary>
        /// Fill a list with a same value.
        /// </summary>
        /// <typeparam name="T">List type data.</typeparam>
        /// <param name="list">Initialized list to fill.</param>
        /// <param name="value">Value to fill each element of list.</param>
        public static void Fill<T>(ref List<T> list, T value)
        {
            list = Enumerable.Repeat<T>(value, list.Count).ToList();
        }

        /// <summary> 
        /// Created a safe random seed for intializing the System.Random class. 
        /// </summary> 
        /// <returns>Return a random seed.</returns> 
        /// <remarks>This functions calculated the seed using the System.Security.Cryptography.RandomNumberGenerator.</remarks> 
        public static int CalculateSafeRandomSeed()
        {
            var cryptoResult = new byte[4];
            new RNGCryptoServiceProvider().GetBytes(cryptoResult);
            return BitConverter.ToInt32(cryptoResult, 0);
        }

        /// <summary> 
        /// Suffle an array. 
        /// </summary> 
        /// <typeparam name="T">Type of the array elements.</typeparam> 
        /// <param name="array">Array of elements.</param> 
        public static void Shuffle<T>(ref T[] array)
        {
            var rng = new System.Random(CalculateSafeRandomSeed());
            int n = array.Length;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = array[k];
                array[k] = array[n];
                array[n] = value;
            }
        }

        /// <summary> 
        /// Suffle a list. 
        /// </summary> 
        /// <typeparam name="T">Type of the generic list elements.</typeparam> 
        /// <param name="list">Generic list of elements.</param> 
        public static void Shuffle<T>(ref List<T> list)
        {
            var rng = new System.Random(CalculateSafeRandomSeed());
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        /// <summary>
        /// Transform the enum name element in to string value.
        /// </summary>
        /// <param name="value">Enum value.</param>
        /// <returns>Return the string name value.</returns>
        public static string EnumToString(object value)
        {
            return Enum.GetName(value.GetType(), value);
        }

        /// <summary>
        /// Generate a int32 value based on a GUID value.
        /// </summary>
        /// <returns></returns>
        public static int GenerateInt32GuidValue()
        {
            return BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0);
        }
    }
}
