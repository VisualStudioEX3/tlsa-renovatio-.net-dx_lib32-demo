﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dxlib32_222;

namespace TLSA.Engine.Core.Graphics
{
    /// <summary>
    /// Color structure.
    /// </summary>
    public struct Color
    {
        #region Internal vars
        private int _value;

        private static readonly Color _transparent = new Color(0);
        private static readonly Color _white = new Color(-1);
        #endregion

        #region Properties
        public byte A { get; internal set; }
        public byte R { get; internal set; }
        public byte G { get; internal set; }
        public byte B { get; internal set; }

        public static Color Transparent { get { return _transparent; } }
        public static Color White { get { return _white; } }
        #endregion

        #region Constructors
        public Color(byte alpha, byte red, byte green, byte blue)
        {
            this._value = this.A = this.R = this.G = this.B = 0;
            this.Set(alpha, red, green, blue);
        }

        public Color(int color)
        {
            this._value = this.A = this.R = this.G = this.B = 0;
            this.Set(color);
        }
        #endregion

        #region Operators
        public static bool operator ==(Color a, Color b)
        {
            return a._value == b._value;
        }

        public static bool operator !=(Color a, Color b)
        {
            return !(a == b);
        }

        public static implicit operator Color(int value)
        {
            return new Color(value);
        }

        public static explicit operator int(Color value)
        {
            return value.ToInt32();
        }
        #endregion

        #region Methods & functions
        public void Set(byte alpha, byte red, byte green, byte blue)
        {
            _value = TLSA.Engine.Manager.Graphics.dx_gfx.ARGB_Set(alpha, red, green, blue);
            A = alpha; R = red; G = green; B = blue;
        }

        public void Set(int color)
        {
            var data = new ARGB();
            _value = color;
            TLSA.Engine.Manager.Graphics.dx_gfx.ARGB_Get(color, data);
            A = (byte)data.Alpha; R = (byte)data.Red; G = (byte)data.Green; B = (byte)data.Blue;
        }

        public override bool Equals(object obj)
        {
            if (obj is Color)
            {
                Color objColor = (Color)obj;
                return objColor == this;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return (int)(this.A + this.R + this.G + this.B) % Int32.MaxValue;
        }

        public int ToInt32()
        {
            return _value;
        }

        public override string ToString()
        {
            return string.Format("( Color: {4} (Alpha: {0}, Red: {1}, Green: {2}, Blue: {3}) )", A, R, G, B, ToInt32());
        }

        public string ToString(bool includeFullClassPath)
        {
            return includeFullClassPath ? string.Format("{0}: {1}", this.GetType().FullName, this.ToString()) : this.ToString();
        }
        #endregion
    }
}
