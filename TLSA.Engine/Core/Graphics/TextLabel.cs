﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core.Graphics
{
    /// <summary>
    /// Text label.
    /// </summary>
    public class TextLabel : ComponentBase
    {
        #region Enumerations
        /// <summary>
        /// Text aligns.
        /// </summary>
        public enum TextAlign
        {
            Left = dxlib32_222.Text_Align.Align_Left,
            Right = dxlib32_222.Text_Align.Align_Right,
            Center = dxlib32_222.Text_Align.Align_Center
        }
        #endregion

        #region Properties
        public Font Font { get; set; }
        public Color Color { get; set; }
        public string Text { get; set; }
        public Point2D Position { get; set; }
        public TextAlign Align { get; set; }
        #endregion

        #region Constructors
        public TextLabel(Font font, Color color, Point2D position, string text = "", TextAlign align = TextAlign.Left)
        {
            this.Font = font;
            this.Color = color;
            this.Position = position;
            this.Text = text;
            this.Align = align;
        }
        #endregion

        #region Methods & functions
        public override void Draw()
        {
            TLSA.Engine.Manager.Graphics.dx_gfx.DRAW_Text(this.Font._id, this.Text, (int)this.Position.X, (int)this.Position.Y, (int)this.Position.Z, this.Color.ToInt32(), (dxlib32_222.Text_Align)this.Align);
        }

        public override string ToString()
        {
            return string.Format("( Font: {0}, Color: {1}, Text: {2}, Position: {3}, Align: {4} )",
                                 this.Font, this.Color, this.Text, this.Position, Helper.EnumToString(this.Align));
        }

        public string ToString(bool includeFullClassPath)
        {
            return includeFullClassPath ? string.Format("{0}: {1}", this.GetType().FullName, this.ToString()) : this.ToString();
        }
        #endregion
    }
}
