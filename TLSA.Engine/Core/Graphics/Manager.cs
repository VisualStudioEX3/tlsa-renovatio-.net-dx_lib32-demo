﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dxlib32_222;

namespace TLSA.Engine.Core.Graphics
{
    /// <summary>
    /// Graphics manager.
    /// </summary>
    public class Manager : IDisposable
    {
        #region Enums
        /// <summary>
        /// Image quality values.
        /// </summary>
        public enum QualityFilter
        {
            Point = dxlib32_222.Blit_Filter.Filter_None,
            Bilinear = dxlib32_222.Blit_Filter.Filter_Bilinear,
            Trilinear = dxlib32_222.Blit_Filter.Filter_Trilinear
        } 
        #endregion

        #region Internal vars
        internal dx_GFX_Class dx_gfx;
        #endregion

        #region Properties
        // Antiliasing NOT WORKING! (possible because d3d8 antiliasing extension not compatible with the Intel GMA 950 and actual hardware. Not is an important feature.)
        //public bool Antialiasing
        //{
        //    get { return dx_gfx.Antialiasing; }
        //    set { dx_gfx.DEVICE_SetAntialiasing(value); }
        //}

        /// <summary>
        /// Get if the render is on fullscreen mode, or set to fullscreen.
        /// </summary>
        public bool FullScreen
        {
            get { return !dx_gfx.Windowed; }
            set { dx_gfx.DEVICE_SetDisplayMode(dx_gfx.Screen.Width, dx_gfx.Screen.Height, 32, (value ? false : true), true, true); }
        }

        /// <summary>
        /// Get or set the quality filter on render pipeline.
        /// </summary>
        public QualityFilter Filter { get; set; }
        #endregion

        #region Constructor & destructor
        internal Manager(int hWnd, int width, int height)
        {
            dx_gfx = new dx_GFX_Class();
            if (dx_gfx.Init(hWnd, width, height, 32, true, true, true))
            {
                this.Filter = QualityFilter.Point;
            }
            else
            {
                throw new Exception("Error to trying to initialize dx_GFX class!");
            }
        }
        
        public void Dispose()
        {
            dx_gfx.Terminate();
        }
        #endregion

        #region Methods & functions
        /// <summary>
        /// Open render pipeline.
        /// </summary>
        public void Begin()
        {

        }

        /// <summary>
        /// Close render pipeline and present to screen.
        /// </summary>
        public void End()
        {
            int error = dx_gfx.Frame(0, 60);

            //switch (error)
            //{
            //    case (int)GFX_ErrorCodes.GFX_DEVICELOST:
            //        System.Diagnostics.Debug.Print(Helper.EnumToString(GFX_ErrorCodes.GFX_DEVICELOST));
            //        break;
            //    case (int)GFX_ErrorCodes.GFX_DEVICENOTRESET:
            //        System.Diagnostics.Debug.Print(Helper.EnumToString(GFX_ErrorCodes.GFX_DEVICENOTRESET));
            //        break;
            //    case (int)GFX_ErrorCodes.GFX_UNKNOWNERROR:
            //        System.Diagnostics.Debug.Print(Helper.EnumToString(GFX_ErrorCodes.GFX_UNKNOWNERROR));
            //        break;
            //    case (int)GFX_ErrorCodes.GFX_OK:
            //        System.Diagnostics.Debug.Print(Helper.EnumToString(GFX_ErrorCodes.GFX_OK));
            //        break;
            //}
        }
        #endregion
    }
}
