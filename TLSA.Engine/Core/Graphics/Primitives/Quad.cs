﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core.Graphics.Primitives
{
    /// <summary>
    /// Draw a quad.
    /// </summary>
    /// <remarks>The vertex order is A (Left/Top), B (Right/Top), C (Left/Bottom) and D (Right/Bottom).</remarks>
    public class Quad : ComponentBase
    {
        #region Internal vars
        private dxlib32_222.Vertex[] _vertexArray = new dxlib32_222.Vertex[4];
        internal bool _hasChanges = false;
        #endregion

        #region Properties
        /* Draw order:
         * 
         * A ---> B
         *       /
         *      /
         *     /
         *    /
         *   /
         *  / 
         * C ---> D
         * 
         */
        
        /// <summary>
        /// Left/Top vertex.
        /// </summary>
        public Vertex A { get; set; }
        /// <summary>
        /// Right/Top vertex.
        /// </summary>
        public Vertex B { get; set; }
        /// <summary>
        /// Left/Bottom vertex.
        /// </summary>
        public Vertex C { get; set; }
        /// <summary>
        /// Right/Bottom vertex.
        /// </summary>
        public Vertex D { get; set; }
        #endregion

        #region Constructors
        public Quad()
        {
            this.A = this.B = this.C = this.D = Vertex.Zero;
            this._hasChanges = true;
        }

        public Quad(Vertex a, Vertex b, Vertex c, Vertex d)
        {
            this.A = a; this.A.SetOwner(this);
            this.B = b; this.B.SetOwner(this);
            this.C = c; this.C.SetOwner(this);
            this.D = d; this.D.SetOwner(this);
            this._hasChanges = true;
        }
        #endregion

        #region Methods & functions
        public override void Update()
        {
            if (this._hasChanges)
            {
                this._vertexArray[0] = this.A.ToDxLib32Vertex();
                this._vertexArray[1] = this.B.ToDxLib32Vertex();
                this._vertexArray[2] = this.C.ToDxLib32Vertex();
                this._vertexArray[3] = this.D.ToDxLib32Vertex();
                this._hasChanges = false;
            }
        }

        public override void Draw()
        {
            TLSA.Engine.Manager.Graphics.dx_gfx.DEVICE_SetSpecularChannel((int)this.A.Specular, (int)this.B.Specular, (int)this.C.Specular, (int)this.D.Specular);
            TLSA.Engine.Manager.Graphics.dx_gfx.DRAW_Trapezoid(this._vertexArray);
        }

        public override string ToString()
        {
            return string.Format("( A: {0}, B: {1}, C: {2}, D: {3} )", this.A.ToString(), this.B.ToString(), this.C.ToString(), this.D.ToString());
        }

        public string ToString(bool includeFullClassPath)
        {
            return includeFullClassPath ? string.Format("{0}: {1}", this.GetType().FullName, this.ToString()) : this.ToString();
        }
        #endregion
    }
}
