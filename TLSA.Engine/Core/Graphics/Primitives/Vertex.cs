﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core.Graphics.Primitives
{
    /// <summary>
    /// Vertex structure. Mainly used by <see cref="Quad"/>Quad</see> class.
    /// </summary>
    public struct Vertex
    {
        #region Internal vars
        private static readonly Vertex _zero = new Vertex(Point2D.Zero, Color.Transparent, Color.Transparent);
        internal Quad _quadOwner;

        private float _x, _y;
        private Color _color, _specular;
        #endregion

        #region Properties
        public float X { get { return this._x; } set { this._x = value; this.HasChanges(); } }
        public float Y { get { return this._y; } set { this._y = value; this.HasChanges(); } }
        public Color Color { get { return this._color; } set { this._color = value; this.HasChanges(); } }
        /// <summary>
        /// Specular channel. Use it for iluminate the base color.
        /// </summary>
        public Color Specular { get { return this._specular; } set { this._specular = value; this.HasChanges(); } }

        public static Vertex Zero { get { return _zero; } }
        #endregion

        #region Constructors
        public Vertex(float x, float y, Color color, Color specular)
        {
            this._quadOwner = null;
            this._x = this._y = 0;
            this._color = this._specular = 0;

            this.X = x;
            this.Y = y;
            this.Color = color;
            this.Specular = specular;
        }

        public Vertex(Point2D position, Color color, Color specular)
        {
            this._quadOwner = null;
            this._x = this._y = 0;
            this._color = this._specular = 0;

            this.X = position.X;
            this.Y = position.Y;
            this.Color = color;
            this.Specular = specular;
        }
        #endregion

        #region Operators
        public static bool operator ==(Vertex a, Vertex b)
        {
            return
            (
                 System.Math.Abs(a.X - b.X) <= float.Epsilon &&
                 System.Math.Abs(a.Y - b.Y) <= float.Epsilon &&
                 a.Color == b.Color &&
                 a.Specular == b.Specular
            );
        }

        public static bool operator !=(Vertex a, Vertex b)
        {
            return !(a == b);
        }
        #endregion

        #region Methods & functions
        public void Set(float x, float y)
        {
            this.X = x; this.Y = y;
        }

        public void Set(float x, float y, Color color)
        {
            this.Set(x, y);
            this.Color = color;
        }

        public void Set(float x, float y, Color color, Color specular)
        {
            this.Set(x, y, color);
            this.Specular = specular;
        }

        public void Set(Point2D position, Color color, Color specular)
        {
            this.Set(position.X, position.Y, color, specular);
        }

        public void SetColor(Color color)
        {
            this.Color = color;
        }

        public void SetSpecular(Color specular)
        {
            this.Specular = specular;
        }

        internal void SetOwner(Quad owner)
        {
            this._quadOwner = owner;
        }

        private void HasChanges()
        {
            if (this._quadOwner != null)
                this._quadOwner._hasChanges = true;
        }

        public override bool Equals(object obj)
        {
            if (obj is Vertex)
            {
                Vertex _obj = (Vertex)obj;
                return _obj == this;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return (int)(this.X + this.Y + (int)this.Color + (int)this.Specular) % Int32.MaxValue;
        }

        public override string ToString()
        {
            return string.Format("( X: {0}, Y: {1}, Color: {2}, Specular: {3} )", this.X, this.Y, this.Color, this.Specular);
        }

        public string ToString(bool includeFullClassPath)
        {
            return includeFullClassPath ? string.Format("{0}: {1}", this.GetType().FullName, this.ToString()) : this.ToString();
        }

        internal dxlib32_222.Vertex ToDxLib32Vertex()
        {
            var vertex = new dxlib32_222.Vertex();
            vertex.X = (int)this.X;
            vertex.Y = (int)this.Y;
            vertex.Z = 0;
            vertex.Color = (int)this.Color;
            return vertex;
        }
        #endregion
    }
}
