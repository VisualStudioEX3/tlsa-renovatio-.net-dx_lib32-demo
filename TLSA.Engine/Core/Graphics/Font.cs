﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dxlib32_222;

namespace TLSA.Engine.Core.Graphics
{
    /// <summary>
    /// True Type Font.
    /// </summary>
    public class Font : IDisposable
    {
        #region Internal vars
        internal int _id;
        #endregion

        #region Properties
        public string Name { get; internal set; }
        public uint Size { get; internal set; }
        public bool Bold { get; internal set; }
        public bool Italic { get; internal set; }
        public bool Underline { get; internal set; }
        public bool StrikeThrough { get; internal set; }
        /// <summary>
        /// Indicate is the font is installed on Windows Font system folder.
        /// </summary>
        public bool IsSystemFont { get; internal set; }
        #endregion

        #region Constructor and destructor
        private Font()
        {
            // Empty constructor used in LoadFromFile() static function.
        }

        public Font(string name, uint size, bool bold = false, bool italic = false, bool underline = false, bool strikeThrough = false)
        {
            this._id = TLSA.Engine.Manager.Graphics.dx_gfx.FONT_LoadSystemFont(name, (int)size, bold, italic, underline, strikeThrough);
            if (this._id == (int)dxlib32_222.GFX_ErrorCodes.GFX_OK)
            {
                this.Name = name;
                this.Size = size;
                this.Bold = bold;
                this.Italic = italic;
                this.Underline = underline;
                this.StrikeThrough = strikeThrough;
                this.IsSystemFont = true;
            }
            else
            {
                throw new Exception(string.Format("Error to load the system font '{0}'!", name));
            }
        }

        public void Dispose()
        {
            TLSA.Engine.Manager.Graphics.dx_gfx.FONT_UnloadSystemFont(this._id);
        }
        #endregion

        #region Methods & functions
        public static Font LoadFromFile(string filename, uint size, bool bold = false, bool italic = false, bool underline = false, bool strikeThrough = false)
        {
            var font = new Font();
            string fontName = "";

            font._id = TLSA.Engine.Manager.Graphics.dx_gfx.FONT_LoadSystemFontFromFile(filename, fontName, (int)size, bold, italic, underline, strikeThrough);
            if (font._id == (int)dxlib32_222.GFX_ErrorCodes.GFX_OK)
            {
                font.Name = fontName;
                font.Size = size;
                font.Bold = bold;
                font.Italic = italic;
                font.Underline = underline;
                font.StrikeThrough = strikeThrough;
                font.IsSystemFont = false;
                return font;
            }
            else
            {
                throw new Exception(string.Format("Error to load the font from the filename '{0}'!", filename));
            }
        }

        /// <summary>
        /// Return the bound size area occuped by the rendered string text.
        /// </summary>
        /// <param name="text">String to measured.</param>
        /// <returns>Return the Width and Height of the rendered string.</returns>
        public Point2D GetStringBounds(string text)
        {
            return new Point2D(TLSA.Engine.Manager.Graphics.dx_gfx.FONT_SystemGetTextWidth(this._id, text),
                               TLSA.Engine.Manager.Graphics.dx_gfx.FONT_SystemGetTextHeight(this._id, text));
        }

        public override string ToString()
        {
            return string.Format("( Name: '{0}', Size: {1}, Bold: {2}, Italic: {3}, Underline: {4}, StrikeThrough: {5}, Is a System Font?: {6} )", Name, Size, Bold, Italic, Underline, StrikeThrough);
        }

        public string ToString(bool includeFullClassPath)
        {
            return includeFullClassPath ? string.Format("{0}: {1}", this.GetType().FullName, this.ToString()) : this.ToString();
        }
        #endregion
    }
}
