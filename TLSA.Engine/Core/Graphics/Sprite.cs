﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core.Graphics
{
    /// <summary>
    /// Sprite.
    /// </summary>
    public class Sprite : ComponentBase
    {
        #region Enums
        /// <summary>
        /// Mirror flip values.
        /// </summary>
        public enum SpriteMirror
        {
            None = dxlib32_222.Blit_Mirror.Mirror_None,
            Horizontal = dxlib32_222.Blit_Mirror.Mirror_Horizontal,
            Vertical = dxlib32_222.Blit_Mirror.Mirror_Vertical,
            Both = dxlib32_222.Blit_Mirror.Mirror_both
        }

        /// <summary>
        /// Sprite Effects (Non shader required)
        /// </summary>
        public enum SpriteFX
        {
            /// <summary>
            /// Default effect. Render using the sprite base color.
            /// </summary>
            Color = dxlib32_222.Blit_Alpha.Blendop_Color,
            /// <summary>
            /// Aditive effect. Pixels with color near to black are most transparents, pixels with color near to white are most solid.
            /// </summary>
            Aditive = dxlib32_222.Blit_Alpha.Blendop_Aditive,
            /// <summary>
            /// Sustrative effect. Similar to Additive but most darker.
            /// </summary>
            Sustrative = dxlib32_222.Blit_Alpha.Blendop_Sustrative,
            /// <summary>
            /// Inverse effect. Invert all colors.
            /// </summary>
            Inverse = dxlib32_222.Blit_Alpha.Blendop_Inverse,
            /// <summary>
            /// XOR effect. Inverse the texture pixel color respect the screen pixel color. Black always drawn as transparent color.
            /// </summary>
            XOR = dxlib32_222.Blit_Alpha.Blendop_XOR,
            /// <summary>
            /// Crystaline. Simulates a transluced cristal material. Black always drawn as solid color.
            /// </summary>
            Crystaline = dxlib32_222.Blit_Alpha.Blendop_Crystaline,
            /// <summary>
            /// GreyScale effect. Draws the texture in black & white. When applying alpha transparecy in the base sprite color, the black color always drawn as solid color.
            /// </summary>
            GreyScale = dxlib32_222.Blit_Alpha.Blendop_GreyScale
        }
        #endregion

        #region Properties
        public Point2D Position { get; set; }
        public float Angle { get; set; }
        public Texture Texture { get; set; }
        public Color Color { get; set; }
        public SpriteMirror Mirror { get; set; }
        public SpriteFX Effect { get; set; }
        /// <summary>
        /// Define the anchor of the sprite, used to rotations and draw origin.
        /// </summary>
        public Point2D Center { get; set; }
        /// <summary>
        /// Force to draw centered.
        /// </summary>
        public bool ForceDrawCenter { get; set; }
        /// <summary>
        /// Define the region of the texture to render by the sprite. Empty frame draws the entire texture.
        /// </summary>
        public Rect Frame { get; set; }
        #endregion

        #region Constructor
        public Sprite(Texture texture = null)
        {
            this.Position = Point2D.Zero;
            this.Angle = 0f;
            this.Texture = texture;
            this.Color = Color.Transparent;
            this.Mirror = SpriteMirror.None;
            this.Effect = SpriteFX.Color;
            this.Frame = Rect.Empty;
            this.Center = Point2D.Zero;
            this.ForceDrawCenter = false;
        }
        #endregion

        #region Methods & Functions
        public override void Draw()
        {
            if (this.Texture != null)
            {
                if (this.Frame != Rect.Empty)
                {
                    TLSA.Engine.Manager.Graphics.dx_gfx.MAP_SetRegion(this.Texture._id, this.Frame.ToDxLib32GFXRect());
                }

                TLSA.Engine.Manager.Graphics.dx_gfx.DEVICE_SetDrawCenter((int)this.Center.X, (int)this.Center.Y);

                TLSA.Engine.Manager.Graphics.dx_gfx.DRAW_MapEx(this.Texture._id,
                                                               (int)this.Position.X, (int)this.Position.Y, (int)this.Position.Z,
                                                               0, 0,
                                                               this.Angle,
                                                               (dxlib32_222.Blit_Alpha)this.Effect,
                                                               (int)this.Color,
                                                               (dxlib32_222.Blit_Mirror)this.Mirror,
                                                               (dxlib32_222.Blit_Filter)TLSA.Engine.Manager.Graphics.Filter,
                                                               this.ForceDrawCenter); 
            }
        }

        /// <summary>
        /// Return the final vertex transformation of the sprite.
        /// </summary>
        /// <returns>Point2D array with the final vertex.</returns>
        /// <remarks>This function non render the sprite. Only calculate the final transformation.</remarks>
        public Point2D[] GetTransformVertex()
        {
            dxlib32_222.Vertex[] vertex = new dxlib32_222.Vertex[4];
            Point2D[] points = new Point2D[4];

            TLSA.Engine.Manager.Graphics.dx_gfx.PRECAL_WriteSpriteTransformVertex();
            {
                this.Draw();
            }
            TLSA.Engine.Manager.Graphics.dx_gfx.PRECAL_ReadSpriteTransformVertex(vertex[0], vertex[1], vertex[2], vertex[3]);

            for (int i = 0; i < 4; i++)
            {
                points[i].X = vertex[i].X;
                points[i].Y = vertex[i].Y;
                points[i].Z = vertex[i].Z;
            }

            return points;
        }

        public override string ToString()
        {
            return string.Format("( Position: {0}, Angle: {1}, Texture: {2}, Color: {3}, Mirror: {4}, Effect: {5}, Center: {6}, ForceDrawCenter: {7}, Frame: {8})",
                                 this.Position, this. Angle, this.Texture, this.Color, Helper.EnumToString(this.Mirror), Helper.EnumToString(this.Effect), this.Center, this.ForceDrawCenter, this.Frame);
        }

        public string ToString(bool includeFullClassPath)
        {
            return includeFullClassPath ? string.Format("{0}: {1}", this.GetType().FullName, this.ToString()) : this.ToString();
        }
        #endregion
    }
}
