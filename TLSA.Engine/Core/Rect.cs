﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core
{
    /// <summary>
    /// Rectangle structure.
    /// </summary>
    public struct Rect
    {
        #region Internal vars
        private static readonly Rect _empty = new Rect();
        #endregion

        #region Properties
        public float X { get; set; }
        public float Y { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
        public float Left { get { return this.X - (this.Width / 2); } }
        public float Top { get { return this.Y - (this.Height / 2); } }
        public float Right { get { return this.X + (this.Width / 2); } }
        public float Bottom { get { return this.Y + (this.Height / 2); } }
        /// <summary>
        /// Get or set the center of this rectangle.
        /// </summary>
        public Point2D Center
        {
            get { return new Point2D(this.X, this.Y); }
            set { this.X = value.X; this.Y = value.Y; }
        }
        /// <summary>
        /// Get or set the size of this rectangle.
        /// </summary>
        public Point2D Size
        {
            get { return new Point2D(this.Width, this.Height); }
            set { this.Width = value.X; this.Height = value.Y; }
        }
        public Point2D LeftTop { get { return new Point2D(this.Left, this.Top); } }
        public Point2D RightTop { get { return new Point2D(this.Right, this.Top); } }
        public Point2D LeftBottom { get { return new Point2D(this.Left, this.Bottom); } }
        public Point2D RightBottom { get { return new Point2D(this.Right, this.Bottom); } }

        /// <summary>
        /// Rect(0, 0, 0, 0)
        /// </summary>
        public static Rect Empty { get { return _empty; } }
        #endregion

        #region Constructors
        public Rect(float x, float y, float width, float height)
        {
            this.X = x; this.Y = y; this.Width = width; this.Height = height;            
        }

        public Rect(Point2D center, Point2D size)
        {
            this.X = this.Y = this.Width = this.Height = 0;
            this.Center = center; this.Size = size;            
        }
        #endregion

        #region Operators
        public static bool operator ==(Rect a, Rect b)
        {
            return (a.Center == b.Center) && (a.Size == b.Size);
        }

        public static bool operator !=(Rect a, Rect b)
        {
            return !(a == b);
        }
        #endregion

        #region Static Methods & Functions
        /// <summary>
        /// Create a rect from a bound structure.
        /// </summary>
        /// <param name="left">Left side.</param>
        /// <param name="top">Top side.</param>
        /// <param name="right">Right side.</param>
        /// <param name="bottom">Bottom side.</param>
        /// <returns>The new rect structure.</returns>
        public static Rect CreateRectFromBounds(float left, float top, float right, float bottom)
        {
            float _width = left + right;
            float _height = top + bottom;
            return new Rect(left + (_width / 2), right + (_height / 2), _width, _height);
        }

        /// <summary>
        /// Create a rect from a bound structure.
        /// </summary>
        /// <param name="bounds">Bound structure.</param>
        /// <returns>The new rect structure.</returns>
        public static Rect CreateRectFromBounds(Bounds bounds)
        {
            return CreateRectFromBounds(bounds.Left, bounds.Top, bounds.Right, bounds.Bottom);
        }

        /// <summary>
        /// Determine if a point is inside the rectangle.
        /// </summary>
        /// <param name="rect">Rect structure.</param>
        /// <param name="x">X coordinate of point.</param>
        /// <param name="y">Y coordinate of point.</param>
        /// <returns>Return true if the point is inside the rect structure.</returns>
        public static bool PointInRect(Rect rect, float x, float y)
        {
            return (x >= rect.Left && x <= rect.Right) ||
                   (y >= rect.Top && y <= rect.Bottom);
        }

        /// <summary>
        /// Determine if a point is inside the rectangle.
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="point"></param>
        /// <returns>Return true if the point is inside the rect structure.</returns>
        public static bool PointInRect(Rect rect, Point2D point)
        {
            return PointInRect(rect, point.X, point.Y);
        }

        /// <summary>
        /// Determine if the first rectangle intersect the second rectangle.
        /// </summary>
        /// <param name="a">First rect structure.</param>
        /// <param name="b">Second rect structure.</param>
        /// <returns>Return true if there has intersection.</returns>
        public static bool Intersect(Rect a, Rect b)
        {
            return PointInRect(a, b.LeftTop) ||
                   PointInRect(a, b.RightTop) ||
                   PointInRect(a, b.LeftBottom) ||
                   PointInRect(a, b.RightBottom);
        }

        /// <summary>
        /// Determine if the first rectangle intersect the second rectangle, and return the rectangle difference.
        /// </summary>
        /// <param name="a">First rect structure.</param>
        /// <param name="b">Second rect structure.</param>
        /// <param name="difference">The rect structure with the differece between rectangles.</param>
        /// <returns>Return true if there has intersection.</returns>
        public static bool Intersect(Rect a, Rect b, out Rect difference)
        {
            difference = Rect._empty;

            bool lt = PointInRect(a, b.LeftTop);
            bool rt = PointInRect(a, b.RightTop);
            bool lb = PointInRect(a, b.LeftBottom);
            bool rb = PointInRect(a, b.RightBottom);

            if (lt && rt && lb && rb)
            {
                difference = b;
            }
            else if ((lt && rt) || (lt && lb) || (lb && rb) || (rt && rb))
            {
                if (lt && rt)
                {
                    difference = Rect.CreateRectFromBounds(b.Left, b.Top, b.Right, a.Bottom);
                }
                else if (lt && lb)
                {
                    difference = Rect.CreateRectFromBounds(b.Left, b.Top, a.Right, b.Bottom);
                }
                else if (lb && rb)
                {
                    difference = Rect.CreateRectFromBounds(b.Left, a.Top, b.Right, b.Bottom);
                }
                else if (rt && rb)
                {
                    difference = Rect.CreateRectFromBounds(a.Left, b.Top, b.Right, b.Bottom);
                }
            }
            else if (lt || rt || lb || rb)
            {
                if (lt)
                {
                    difference = Rect.CreateRectFromBounds(b.Left, b.Top, a.Right, a.Bottom);
                }
                else if (lb)
                {
                    difference = Rect.CreateRectFromBounds(b.Left, a.Top, a.Right, b.Bottom);
                }
                else if (rt)
                {
                    difference = Rect.CreateRectFromBounds(a.Left, b.Top, b.Right, a.Bottom);
                }
                else if (rb)
                {
                    difference = Rect.CreateRectFromBounds(a.Left, a.Top, b.Right, b.Bottom);
                }
            }

            return (lt || rt || lb || rb);
        }
        #endregion

        #region Methods & Functions
        public void Set(float x, float y, float width, float height)
        {
            this.X = x; this.Y = y; this.Width = width; this.Height = height;
        }

        public void SetCenter(float x, float y)
        {
            this.X = x; this.Y = y;
        }
                
        public void SetCenter(Point2D center)
        {
            this.Center = center;
        }

        public void SetSize(float width, float height)
        {
            this.Width = width; this.Height = height;
        }

        public void SetSize(Point2D size)
        {
            this.Size = size;
        }

        /// <summary>
        /// Get the rectangle bounds.
        /// </summary>
        /// <returns>Return the bound structure with the rectangle bounds.</returns>
        public Bounds GetBounds()
        {
            return new Bounds(this.Left, this.Top, this.Right, this.Bottom);
        }
        /// <summary>
        /// Determine if a point is inside this rectangle.
        /// </summary>
        /// <param name="x">X coordinate of point.</param>
        /// <param name="y">Y coordinate of point.</param>
        /// <returns>Return true if the point is inside this rect structure.</returns>
        public bool PointInRect(float x, float y)
        {
            return Rect.PointInRect(this, x, y);
        }

        /// <summary>
        /// Determine if a point is inside this rectangle.
        /// </summary>
        /// <param name="point"></param>
        /// <returns>Return true if the point is inside this rect structure.</returns>
        public bool PointInRect(Point2D point)
        {
            return Rect.PointInRect(this, point);
        }

        /// <summary>
        /// Determine if this rectangle intersect the other rectangle.
        /// </summary>
        /// <param name="rect">Second rect structure.</param>
        /// <returns>Return true if there has intersection.</returns>
        public bool Intersect(Rect rect)
        {
            return Rect.Intersect(this, rect);
        }

        /// <summary>
        /// Determine if this rectangle intersect the other rectangle, and return the rectangle difference.
        /// </summary>
        /// <param name="rect">Second rect structure.</param>
        /// <param name="difference">The rect structure with the differece between rectangles.</param>
        /// <returns>Return true if there has intersection.</returns>
        public bool Intersect(Rect rect, out Rect difference)
        {
            return Rect.Intersect(this, rect, out difference);
        }

        public override int GetHashCode()
        {
            return (int)(this.X + this.Y + this.Width + this.Height + this.Left + this.Top + this.Right + this.Bottom) % Int32.MaxValue;
        }

        public override bool Equals(object obj)
        {
            if (obj is Rect)
            {
                Rect objRect = (Rect)obj;

                return objRect == this;
            }
            else
            {
                return false;
            }
        }
        
        public override string ToString()
        {
            return string.Format("( Center: ( X: {0}, Y: {1} ), Size: ( Width: {2}, Height: {3} ), Bounds: ( Left: {4}, Top: {5}, Right: {6}, Bottom: {7} ) )", this.X, this.Y, this.Width, this.Height, this.Left, this.Top, this.Right, this.Bottom);
        }

        public string ToString(bool includeFullClassPath)
        {
            return includeFullClassPath ? string.Format("{0}: {1}", this.GetType().FullName, this.ToString()) : this.ToString();
        }

        internal dxlib32_222.GFX_Rect ToDxLib32GFXRect()
        {
            var rect = new dxlib32_222.GFX_Rect();

            rect.X = (int)this.Left;
            rect.Y = (int)this.Top;
            rect.Width = (int)this.Width;
            rect.Height = (int)this.Height;

            return rect;
        }
        #endregion
    }
}
