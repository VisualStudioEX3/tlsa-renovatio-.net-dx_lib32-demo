﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace TLSA.Engine.Core
{
    /// <summary>
    /// Point2D structure (similar a vector).
    /// </summary>
    public struct Point2D
    {
        #region Internal vars
        private static readonly Point2D _zero = new Point2D(0f, 0f);
        private static readonly Point2D _one = new Point2D(1f, 1f);
        private static readonly Point2D _oneX = new Point2D(1f, 0f);
        private static readonly Point2D _oneY = new Point2D(0f, 1f);
        #endregion

        #region Properties
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        /// <summary>
        /// Get or set the direction, represented by an angle.
        /// </summary>
        public float Direction { get; set; }

        /// <summary>
        /// Return the normalized Point2D (values between 0 to 1).
        /// </summary>
        public Point2D Normalized
        {
            get
            {
                var normalizePoint = new Point2D(this.X, this.Y, this.Z, this.Direction);
                normalizePoint.Normalize();
                return normalizePoint;
            }
        }

        /// <summary>
        /// Point2D(0, 0, 0, 0)
        /// </summary>
        public static Point2D Zero { get { return _zero; } }
        /// <summary>
        /// Point2D(1, 1, 0, 0)
        /// </summary>
        public static Point2D One { get { return _one; } }
        /// <summary>
        /// Point2D(1, 0, 0, 0)
        /// </summary>
        public static Point2D UnitX { get { return _oneX; } }
        /// <summary>
        /// Point2D(0, 1, 0, 0)
        /// </summary>
        public static Point2D UnitY { get { return _oneY; } }
        #endregion

        #region Constructors
        public Point2D(float x, float y, float z = 0f, float direction = 0f)
        {
            this.X = x; this.Y = y; this.Z = z;

            if (direction >= 360 || direction <= -360)
            {
                this.Direction = 0;
            }
            else
            {
                this.Direction = direction;
            }
        }
        #endregion

        #region Operators
        public static Point2D operator +(Point2D a, Point2D b)
        {
            return new Point2D(a.X + b.X, a.Y + b.Y);
        }

        public static Point2D operator -(Point2D a, Point2D b)
        {
            return new Point2D(a.X - b.X, a.Y - b.Y);
        }

        public static Point2D operator *(Point2D a, Point2D b)
        {
            return new Point2D(a.X * b.X, a.Y * b.Y);
        }

        public static Point2D operator *(Point2D p, float f)
        {
            return new Point2D(p.X * f, p.Y * f);
        }

        public static Point2D operator *(float f, Point2D p)
        {
            return p * f;
        }

        public static Point2D operator /(Point2D a, Point2D b)
        {
            return new Point2D(a.X / b.X, a.Y / b.Y);
        }

        public static Point2D operator /(Point2D p, float f)
        {
            return new Point2D(p.X / f, p.Y / f);
        }

        public static Point2D operator /(float f, Point2D p)
        {
            return p / f;
        }

        public static Point2D operator -(Point2D p)
        {
            return new Point2D(-p.X, -p.Y);
        }

        public static Point2D operator +(Point2D p)
        {
            return new Point2D(+p.X, +p.Y);
        }

        public static bool operator ==(Point2D a, Point2D b)
        {
            return
            (
                MathHelper.Approximately(a.X, b.X) &&
                MathHelper.Approximately(a.Y, b.Y)
            );
        }

        public static bool operator !=(Point2D a, Point2D b)
        {
            return !(a == b);
        }
        #endregion

        #region Methods & functions
        public void Set(float x, float y)
        {
            this.X = x; this.Y = y;
        }

        public void Set(float x, float y, float z)
        {
            this.Set(x, y);
            this.Z = z;
        }

        public void Set(float x, float y, float z, float direction)
        {
            this.Set(x, y, z);
            this.Direction = direction;
        }

        public void Set(Point2D position, float direction)
        {
            this.Set(position.X, position.Y, position.Z, direction);
        }

        public void SetDirection(float direction)
        {
            this.Direction = direction;
        }

        /// <summary>
        /// Get the angle between this and other point.
        /// </summary>
        /// <param name="to">The other point.</param>
        /// <returns>Return the angle between the two points, in degrees.</returns>
        public float GetAngle(Point2D to)
        {
            float angle = MathHelper.ToDegrees((float)System.Math.Atan2(to.Y - this.Y, to.X - this.X));
            return (angle < 0 ? angle + 360 : angle);
        }

        /// <summary>
        /// Get the distance, in pixels, between this and other point.
        /// </summary>
        /// <param name="to">The other point.</param>
        /// <returns>Returns the distance between the two points, in pixels.</returns>
        public float GetDistance(Point2D to)
        {
            float x, y;

            x = System.Math.Abs(to.X - this.X);
            y = System.Math.Abs(to.Y - this.Y);

            return (float)System.Math.Sqrt(x * x + y * y);
        }

        /// <summary>
        /// Move this point to the desired coordinates.
        /// </summary>
        /// <param name="x">X coordinate.</param>
        /// <param name="y">Y coordinate.</param>
        public void Move(float x, float y)
        {
            X = x; Y = y;
        }

        /// <summary>
        /// Move this point to the desired coordinates.
        /// </summary>
        /// <param name="point">Point coordinates.</param>
        public void Move(Point2D point)
        {
            X = point.X; Y = point.Y;
        }

        /// <summary>
        /// Move this point in the current direction.
        /// </summary>
        /// <param name="distance">Distance in pixels.</param>
        public void Translate(float distance)
        {
            float cosAngle = (float)Math.Cos(MathHelper.ToRadians(this.Direction));
            this.X = this.X + distance * cosAngle;
            this.Y = this.Y + distance * cosAngle;
        }

        /// <summary>
        /// Interpolate between this point and other point coordinates.
        /// </summary>
        /// <param name="to">The other point.</param>
        /// <param name="speed">Speed interpolation factor.</param>
        public void Lerp(Point2D to, float speed)
        {
            this.X = MathHelper.Lerp(this.X, to.X, speed);
            this.Y = MathHelper.Lerp(this.Y, to.Y, speed);
        }

        /// <summary>
        /// Normalize the current X and Y values of this point (between 0 to 1).
        /// </summary>
        public void Normalize()
        {
            float val = 1.0f / (float)Math.Sqrt((this.X * this.X) + (this.Y * this.Y));
            this.X *= val;
            this.Y *= val;
        }

        /// <summary>
        /// Dot product between points.
        /// </summary>
        /// <param name="secondPoint">The second point.</param>
        /// <returns></returns>
        public float Dot(Point2D secondPoint)
        {
            return (this.X * secondPoint.X) + (this.Y * secondPoint.Y);
        }

        public override int GetHashCode()
        {
            return (int)(this.X + this.Y + this.Z + this.Direction) % Int32.MaxValue;
        }

        public override bool Equals(object obj)
        {
            if (obj is Point2D)
            {
                Point2D objPoint2D = (Point2D)obj;

                return objPoint2D == this;
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return string.Format("( X: {0}, Y: {1}, Z: {2}, Direction: {3}º )", X, Y, Z, Direction);
        }

        public string ToString(bool includeFullClassPath)
        {
            return includeFullClassPath ? string.Format("{0}: {1}", this.GetType().FullName, this.ToString()) : this.ToString();
        }

        internal dxlib32_222.Vertex ToDxLib32Vertex()
        {
            var vertex = new dxlib32_222.Vertex();

            vertex.X = (int)X;
            vertex.Y = (int)Y;
            vertex.Z = (int)Z;

            return vertex;
        }
        #endregion
    }
}
