﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dxlib32_222;

namespace TLSA.Engine.Core.Audio
{
    public class Manager : IDisposable
    {
        internal dx_Sound_Class dx_audio;

        internal Manager(int hWnd)
        {
            dx_audio = new dx_Sound_Class();
            dx_audio.Init(hWnd, 64);
        }

        public void Dispose()
        {
            dx_audio.Terminate();
        }
    }
}
