﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace TLSA.Engine.Core.IO.Zip
{
    /// <summary>
    /// ZIP compressed file entry.
    /// </summary>
    public struct ZipEntry
    {
        #region Internal vars
        internal System.IO.Compression.ZipArchiveEntry _entry;
        #endregion

        #region Properties
        public string Filename { get; internal set; }
        public long Size { get; internal set; }
        #endregion

        #region Constructors
        internal ZipEntry(System.IO.Compression.ZipArchiveEntry entry)
        {
            this._entry = entry;
            this.Filename = entry.FullName;
            this.Size = entry.Length;
        } 
        #endregion
    }
}
