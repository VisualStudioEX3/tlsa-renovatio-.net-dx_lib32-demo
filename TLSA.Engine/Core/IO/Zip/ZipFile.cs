﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace TLSA.Engine.Core.IO.Zip
{
    /// <summary>
    /// ZIP compressed file.
    /// </summary>
    public class ZipFile : IDisposable
    {
        #region Internal vars
        private System.IO.Compression.ZipArchive _zipFile;
        #endregion

        #region Properties
        public string Filename { get; internal set; }
        public ReadOnlyCollection<ZipEntry> Files { get; internal set; }
        #endregion

        #region Constructor & destructor
        public ZipFile(string filename)
        {
            this.Filename = filename;

            this._zipFile = new System.IO.Compression.ZipArchive(new FileStream(filename, FileMode.Open), ZipArchiveMode.Read);

            var entries = new List<ZipEntry>();
            foreach (var file in this._zipFile.Entries)
            {
                entries.Add(new ZipEntry(file));
            }
            this.Files = entries.AsReadOnly();
        }
        
        public void Dispose()
        {
            this._zipFile.Dispose();
        }
        #endregion

        #region Methods & functions
        public void ExtractFile(ZipEntry fileToExtract, string destinationFileName)
        {
            fileToExtract._entry.ExtractToFile(destinationFileName);
        }

        public byte[] ExtractFileToMemory(ZipEntry fileToExtract)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                fileToExtract._entry.Open().CopyTo(ms);
                return ms.ToArray();
            }
        } 
        #endregion
    }
}