﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dxlib32_222;

namespace TLSA.Engine.Core.Video
{
    public class Manager : IDisposable
    {
        internal dx_Video_Class dx_video;

        public Manager(int hWnd)
        {
            dx_video = new dx_Video_Class();
            dx_video.Init(hWnd);
        }

        public void Dispose()
        {
            dx_video.Terminate();
        }
    }
}
