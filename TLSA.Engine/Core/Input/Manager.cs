﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dxlib32_222;

namespace TLSA.Engine.Core.Input
{
    public class Manager : IDisposable
    {
        internal dx_Input_Class dx_input;

        internal Manager(int hWnd)
        {
            dx_input = new dx_Input_Class();
            dx_input.Init(hWnd);
        }

        public void Dispose()
        {
            dx_input.Terminate();
        }

        public void Update()
        {
            dx_input.Update(); 
        }
    }
}
