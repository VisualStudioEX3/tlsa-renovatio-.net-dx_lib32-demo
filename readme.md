TLSA Project (Renovatio)
========================

#### A 2D cinematic platform game demo developed in .NET/C# using dx_lib32 Project.

A sci-fy 2D cinematic platformer demo developed in __.NET__, __C#__ and __dx_lib32 Project__ (only for Windows). 

Demo based on abadoned work, material and designs relative to __TLSA Project__ during a period between 2002 and 2010 using __dx_lib32 Project__ and __Visual Basic 6.0__ game engine, __TLSA Engine__. 

Not intended to be a complete game, only a demo of few levels.

- dx_lib32 Project: https://portfolio.visualstudioex3.com/2006/02/25/dxlib32-project/
- TLSA Engine (VB6): https://portfolio.visualstudioex3.com/2010/07/30/tlsa-engine-vb6/

![Primitive tests](https://visualstudioex3.com/wp-content/uploads/2017/01/tlsa.engine_2017.jpg)