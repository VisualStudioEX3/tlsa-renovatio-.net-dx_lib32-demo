﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TLSA.Engine;
using TLSA.Engine.Core;
using TLSA.Engine.Core.Graphics;
using TLSA.Engine.Core.Graphics.Primitives;

namespace TLSA
{
    public class Game
    {
        public bool Active { get; set; }
        public GameWindow Window { get; private set; }

        public Game()
        {
            this.Window = new GameWindow();
            this.Window.GameInstance = this;
            this.Window.Show();

            Manager.Initialize(this.Window.Handle.ToInt32(), 800, 480);

            // Fix client size on game window. dx_lib32 fail to apply the right client size on Windows 10:
            Window.ClientSize = new System.Drawing.Size(800, 480);

            this.Active = true;
            
            var font = new Font("Lucida Console", 10);
            var label = new TextLabel(font, Color.White, Point2D.Zero, "Hello, world!");

            var pixelBuffer = new PixelBuffer();
            pixelBuffer.Add(new Point2D(0, 0), Color.Red);
            pixelBuffer.Add(new Point2D(1, 0), Color.Yellow);
            pixelBuffer.Add(new Point2D(2, 0), Color.White);

			var shape_a = new Quad(
                new Vertex(0, 0, Color.Transparent, Color.Transparent),
                new Vertex(400, 0, Color.Transparent, Color.Transparent),
                new Vertex(0, 240, Color.Transparent, Color.Transparent),
                new Vertex(400, 240, Color.White, Color.Transparent));

            var shape_b = new Quad(
                new Vertex(400, 0, Color.Transparent, Color.Transparent),
                new Vertex(600, 120, Color.Transparent, Color.Transparent),
                new Vertex(400, 240, Color.White, Color.Transparent),
                new Vertex(800, 240, Color.Transparent, Color.Transparent));

            var shape_c = new Quad(
                new Vertex(0, 240, Color.Transparent, Color.Transparent),
                new Vertex(400, 240, Color.White, Color.Transparent),
                new Vertex(200, 360, Color.Transparent, Color.Transparent),
                new Vertex(400, 480, Color.Transparent, Color.Transparent));

            var shape_d = new Quad(
                new Vertex(400, 240, Color.White, Color.Transparent),
                new Vertex(800, 240, Color.Transparent, Color.Transparent),
                new Vertex(400, 480, Color.Transparent, Color.Transparent),
                new Vertex(800, 480, Color.Transparent, Color.Transparent));

			var circle = new Circle(new Point2D(200, 200, 0), 32, 1, Color.Red, Color.Transparent);

			var line = new Line(Point2D.Zero, new Point2D(800, 480), Color.Lime);

			while (this.Active)
            {
                label.Text = $"{Manager.Instance.FramesPerSecond.ToString()}fps, {Manager.Instance.DeltaTime.ToString()} delta time";
                pixelBuffer.Update();
                shape_a.Update();
                shape_b.Update();
                shape_c.Update();
                shape_d.Update();
                circle.Update();

                Manager.Instance.Update();

                label.Draw();
                pixelBuffer.Draw();
                shape_a.Draw();
                shape_b.Draw();
                shape_c.Draw();
                shape_d.Draw();
                circle.Draw();
                line.Draw();

                Manager.Instance.Draw();
            }

            try
            {
                this.Window.Close();
            }            
            finally
            {
                TLSA.Engine.Manager.Terminate();
            }            
        }
    }
}
