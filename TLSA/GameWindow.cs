﻿using System;
using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TLSA.Engine;
using TLSA.Engine.Core;
using TLSA.Engine.Core.Graphics;
using TLSA.Engine.Core.Graphics.Primitives;

namespace TLSA
{
    public partial class GameWindow : Form
    {
        public Game GameInstance { get; set; }

        public GameWindow()
        {
            InitializeComponent();
        }

        private void GameWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.GameInstance.Active = false;
        }
    }
}
