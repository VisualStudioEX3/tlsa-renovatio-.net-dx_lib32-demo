﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core
{
    /// <summary>
    /// Timer for calculate and manage intervals.
    /// </summary>
    public class Timer
    {
        #region Internal vars
        private bool _isPaused;
        private float _delta;
        private float _pauseDelta;
        #endregion

        #region Properties
        /// <summary>
        /// Get or set if the timer is paused.
        /// </summary>
        public bool IsPaused
        {
            get
            {
                return this._isPaused;
            }
            set
            {
				if (this._isPaused == value) return;

				this._isPaused = value;

                if (this._isPaused)
                {
                    this._pauseDelta = this.GetTicksInSeconds();
                }
                else
                {
                    this._delta += this.GetTicksInSeconds() - this._pauseDelta;
                }
            }
        }

        /// <summary>
        /// Current time interval in seconds.
        /// </summary>
        public float Time
        {
            get
            {
                return (this.IsPaused ? this._pauseDelta : this.GetTicksInSeconds()) - this._delta;
            }
        } 
        #endregion

        #region Constructors
        public Timer()
        {
            this.Reset();
        } 
        #endregion

        #region Methods & functions
        /// <summary>
        /// Reset the timer.
        /// </summary>
        public void Reset()
        {
            this.IsPaused = false;
            this._delta = GetTicksInSeconds();
        }

        private float GetTicksInSeconds()
        {
			return (Environment.TickCount * 0.001f);
        }

        public override string ToString()
        {
            return $"( Time: {this.Time}, IsPaused: {this.IsPaused} )";
        }

        public string ToString(bool includeFullClassPath)
        {
			return includeFullClassPath ? $"{this.GetType().FullName}: {this.ToString()}" : this.ToString();
		}
        #endregion
    }
}
