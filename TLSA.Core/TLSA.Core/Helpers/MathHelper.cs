﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core
{
    /// <summary>
    /// Math helper class.
    /// </summary>
    public static class MathHelper
    {
        /// <summary>
        /// PI value.
        /// </summary>
        public const float PI = (float)Math.PI;

        /// <summary>
        /// Transform radians to degrees.
        /// </summary>
        /// <param name="angle">Angle in radians.</param>
        /// <returns>Return the angle in degrees.</returns>
        public static float ToDegrees(float angle)
        {
            return angle / PI * 180;
        }

        /// <summary>
        /// Transform the degree to radians.
        /// </summary>
        /// <param name="angle">Angle in degrees.</param>
        /// <returns>Return the angle in radians.</returns>
        public static float ToRadians(float angle)
        {
            return angle * PI / 180;
        }

        /// <summary>
        /// Linear interpolation between two values.
        /// </summary>
        /// <param name="a">First value.</param>
        /// <param name="b">Second value.</param>
        /// <param name="factor">Interpolation factor.</param>
        /// <returns></returns>
        public static float Lerp(float a, float b, float factor)
        {
            return a * factor + b * (1 - factor);
        }

        /// <summary>
        /// Clamp a value between a maximun and minmun range.
        /// </summary>
        /// <param name="val">Value to clamp.</param>
        /// <param name="min">Minimal value range.</param>
        /// <param name="max">Maximal value range.</param>
        /// <returns>Return the clamp value.</returns>
        public static int Clamp(int val, int min, int max)
        {
			return (val.CompareTo(min) < 0) ? min : (val.CompareTo(min) > 0) ? max : val;
		}

        /// <summary>
        /// Clamp a value between a maximun and minmun range.
        /// </summary>
        /// <param name="val">Value to clamp.</param>
        /// <param name="min">Minimal value range.</param>
        /// <param name="max">Maximal value range.</param>
        /// <returns>Return the clamp value.</returns>
        public static float Clamp(float val, float min, float max)
        {
			return (val.CompareTo(min) < 0) ? min : (val.CompareTo(min) > 0) ? max : val;
		}

		/// <summary>
		/// Clamp a value between 0 and 1 range.
		/// </summary>
		/// <param name="val">Value to clamp.</param>
		/// <returns>Return the clamp value.</returns>
		public static float Clamp01(float val)
		{
			return Clamp(0f, 1f, val);
		}

		/// <summary>
		/// Clamp a angle value between 0 and 360 in a positive and negative range.
		/// </summary>
		/// <param name="angle"></param>
		/// <returns>Return the clamp value.</returns>
		public static float ClampAngle(float angle)
		{
			float delta = (angle < 360f || angle > -360f) ? 360f * (float)Math.Truncate(angle / 360f) : 360f;
			return (angle < 0) ? angle + delta : angle - delta;
		}

        /// <summary>
        /// Determine if the float value is near to other float value.
        /// </summary>
        /// <param name="a">First value.</param>
        /// <param name="b">Second value.</param>
        /// <returns>Returns if the first value is near or the same as second value.</returns>
        public static bool Approximately(float a, float b)
        {
            return Math.Abs(a - b) <= float.Epsilon;
        }

        /// <summary>
        /// Calculate the percent from a value and range.
        /// </summary>
        /// <param name="value">Value to percent.</param>
        /// <param name="range">Range max value (e.g. 100)</param>
        /// <returns>Return the percent value.</returns>
        public static float PercentFromValue(int value, int range)
        {
            return value / range * 100;
        }

        /// <summary>
        /// Calculate the percent from a value and range.
        /// </summary>
        /// <param name="value">Value to percent.</param>
        /// <param name="range">Range max value (e.g. 100)</param>
        /// <returns>Return the percent value.</returns>
        public static float PercentFromValue(float value, float range)
        {
            return value / range * 100;
        }

        /// <summary>
        /// Calculate the value from a percent and range.
        /// </summary>
        /// <param name="percent">Percent value.</param>
        /// <param name="range">Range max value (e.g. 100)</param>
        /// <returns>Return the natural value.</returns>
        public static int ValueFromPercent(int percent, int range)
        {
            return percent * range / 100;
        }

        /// <summary>
        /// Calculate the value from a percent and range.
        /// </summary>
        /// <param name="percent">Percent value.</param>
        /// <param name="range">Range max value (e.g. 100)</param>
        /// <returns>Return the natural value.</returns>
        public static float ValueFromPercent(float percent, float range)
        {
            return percent * range / 100;
        }
    }
}
