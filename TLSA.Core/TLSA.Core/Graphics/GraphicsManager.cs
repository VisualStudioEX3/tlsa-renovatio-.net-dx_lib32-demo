﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dxlib32_222;

namespace TLSA.Engine.Core.Graphics
{
    /// <summary>
    /// Graphics manager.
    /// </summary>
    public class GraphicsManager : IDisposable
    {
		#region Singleton
		private static GraphicsManager _instance;

		public static GraphicsManager Instance
		{
			get
			{
				if (_instance == null) _instance = new GraphicsManager();
				return _instance;
			}
		}
		#endregion

		#region Enums
		/// <summary>
		/// Image quality values.
		/// </summary>
		public enum QualityFilter
        {
            Point = dxlib32_222.Blit_Filter.Filter_None,
            Bilinear = dxlib32_222.Blit_Filter.Filter_Bilinear,
            Trilinear = dxlib32_222.Blit_Filter.Filter_Trilinear
        }

        /// <summary>
        /// Graphic device states.
        /// </summary>
        /// <remarks>This states define posible errors in the final step of the render.</remarks>
        public enum GraphicDeviceStates
        {
            Ok = dxlib32_222.GFX_ErrorCodes.GFX_OK,
            DeviceLost = dxlib32_222.GFX_ErrorCodes.GFX_DEVICELOST,
            DeviceNotReset = dxlib32_222.GFX_ErrorCodes.GFX_DEVICENOTRESET,
            Unknowk = dxlib32_222.GFX_ErrorCodes.GFX_UNKNOWNERROR
        }
        #endregion

        #region Internal vars
        internal dx_GFX_Class _dx_gfx_instance;
        TextLabel _fpsLabel;
        #endregion

        #region Properties
        // Antiliasing NOT WORKING! (possible because d3d8 antiliasing extension not compatible with the Intel GMA 950 and actual hardware. Not is an important feature.)
        //public bool Antialiasing
        //{
        //    get { return this._dx_gfx.Antialiasing; }
        //    set { this._dx_gfx.DEVICE_SetAntialiasing(value); }
        //}

        /// <summary>
        /// Get if the render is on fullscreen mode, or set to fullscreen.
        /// </summary>
        public bool FullScreen
        {
            get { this.CheckInitialization(); return !this._dx_gfx_instance.Windowed; }
            set { this.CheckInitialization(); this._dx_gfx_instance.DEVICE_SetDisplayMode(this._dx_gfx_instance.Screen.Width, this._dx_gfx_instance.Screen.Height, 32, (value ? false : true), true, true); }
        }

        /// <summary>
        /// Get or set the quality filter on render pipeline.
        /// </summary>
        public QualityFilter Filter { get; set; }

		/// <summary>
		/// Frame per seconds rate.
		/// </summary>
		public int FPS { get { this.CheckInitialization(); return this._dx_gfx_instance.FPS; } }

        /// <summary>
        /// Maximum frames per second limit.
        /// </summary>
        public int MaxFPS { get; set; }

        /// <summary>
        /// Color used to clear frame.
        /// </summary>
        public Color ClearColor { get; set; }

        /// <summary>
        /// Define if the render clear the backbuffer before the render operation.
        /// </summary>
        public bool ClearFrame { get; set; }

        /// <summary>
        /// Store the device state after the last render operation.
        /// </summary>
        public GraphicDeviceStates DeviceState { get; private set; }

        /// <summary>
        /// Default font (Lucida Console, with size 8 and default style)
        /// </summary>
        public Font DefaultFont { get; private set; }

        /// <summary>
        /// Show the FPS in the left top corner over the final render (default false).
        /// </summary>
        public bool ShowFPS { get; set; }

        /// <summary>
        /// Color used to render the FPS counter (default yellow).
        /// </summary>
        public Color ShowFPSColor { get; set; }
        #endregion

        #region Constructor & destructor
        private GraphicsManager()
        {
            this._dx_gfx_instance = new dx_GFX_Class();
        }
        
        public void Dispose()
        {
			this.CheckInitialization();
			this._dx_gfx_instance.Terminate();
        }
		#endregion

		#region Methods & functions
		private void CheckInitialization()
		{
			if (this._dx_gfx_instance == null) throw new Exception("Manager not initialized.");
		}

		public void Initialize(int hWnd, int width, int height)
        {
            this.CheckInitialization();
            if (this._dx_gfx_instance.Init(hWnd, width, height, 32, true, true, true))
            {
                Color.InitializeColorConstants();
                this.Filter = QualityFilter.Point;
                this.ClearColor = Color.Transparent;
                this.MaxFPS = 60;
                this.ClearFrame = true;
                this.DeviceState = GraphicDeviceStates.Ok;
                this.DefaultFont = new Font("Lucida Console", 8);
                this.ShowFPS = false;
                this.ShowFPSColor = Color.Yellow;
                this._fpsLabel = new TextLabel(this.DefaultFont, this.ShowFPSColor, Point2D.Zero);
            }
            else
            {
                throw new Exception("Error to trying to initialize dx_GFX class!");
			}
        }

        /// <summary>
        /// Open render pipeline.
        /// </summary>
        public void Begin()
        {
			this.CheckInitialization();
			// ...
		}

        /// <summary>
        /// Close render pipeline and present to screen.
        /// </summary>
        public void End()
        {
			this.CheckInitialization();
            this.DrawFPS();
			this.DeviceState = (GraphicDeviceStates)this._dx_gfx_instance.Frame((int)this.ClearColor, (short)this.MaxFPS, this.ClearFrame);
        }

        void DrawFPS()
        {
            if (this.ShowFPS)
            {
                this._fpsLabel.Text = $"{this.FPS}fps";
                this._fpsLabel.Draw();
            }
        }
        #endregion
    }
}
