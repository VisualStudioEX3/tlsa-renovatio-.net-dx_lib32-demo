﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core.Graphics.Primitives
{
    /// <summary>
    /// Primitive line.
    /// </summary>
    public class Line
    {
		#region Internal vars
		private dxlib32_222.dx_GFX_Class _graphics = GraphicsManager.Instance._dx_gfx_instance;
		#endregion

		#region Properties
		public Point2D A { get; set; }
        public Point2D B { get; set; }
        public Color Color { get; set; }
        #endregion

        #region Constructors
        public Line()
        {
            this.A = this.B = Point2D.Zero;
            this.Color = Color.Transparent;
        }

        public Line(Point2D a, Point2D b, Color color)
        {
            this.A = a;
            this.B = b;
            this.Color = color;
        }
        #endregion

        #region Methods & Functions
        public void Draw()
        {
            this._graphics.DRAW_Line((int)this.A.X, (int)this.A.Y, (int)this.B.X, (int)this.B.Y, 0, this.Color.ToInt32());
        }

        public override string ToString()
        {
            return $"( A: {this.A.ToString()}, B: {this.B.ToString()} )";
        }

        public string ToString(bool includeFullClassPath)
        {
			return includeFullClassPath ? $"{this.GetType().FullName}: {this.ToString()}" : this.ToString();
		}
        #endregion
    }
}
