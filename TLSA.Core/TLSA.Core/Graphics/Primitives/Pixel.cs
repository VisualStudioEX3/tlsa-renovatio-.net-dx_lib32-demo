﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core.Graphics.Primitives
{
    /// <summary>
    /// Primitive pixel.
    /// </summary>
    /// <remarks>For drawing a block of pixels, see <see cref="PixelBuffer">PixelBuffer</see>/>class.</remarks>
    public class Pixel : IEquatable<Pixel>
    {
		#region Internal vars
		private dxlib32_222.dx_GFX_Class _graphics = GraphicsManager.Instance._dx_gfx_instance;
		internal PixelBuffer _pixelBufferOwner;
        private Point2D _position;
        private Color _color;
        #endregion

        #region Properties
        public Point2D Position { get { return this._position; } set { this._position = value; this.HasChanges(); } }
        public Color Color { get { return this._color; } set { this._color = value; this.HasChanges(); } }
        #endregion

        #region Constructors
        public Pixel(Point2D position, Color color)
        {
            this._position = position;
            this._color = color;
        }
        #endregion

        #region Operators
        public static bool operator ==(Pixel a, Pixel b)
        {
            return (a.Position == b.Position && a.Color == b.Color);
        }

        public static bool operator !=(Pixel a, Pixel b)
        {
            return !(a == b);
        }
        #endregion

        #region Methods & functions
        private void HasChanges()
        {
            if (this._pixelBufferOwner != null)
                this._pixelBufferOwner._hasChanges = true;
        }

        public void Draw()
        {
            this._graphics.DRAW_Pixel((int)this.Position.X, (int)this.Position.Y, 0, (int)this.Color);
        }

        public void Set(Color color)
        {
            this.Color = color;
            this.HasChanges();
        }

        public void Set(Point2D position, Color color)
        {
            this.Position = position;
            this.Color = color;
            this.HasChanges();
        }

        public void Set(float x, float y)
        {
            this.Position.Set(x, y);
			this.HasChanges();
        }

		public void Set(float x, float y, Color color)
		{
			this.Position.Set(x, y);
			this.Color = color;
			this.HasChanges();
		}

        public void Move(float distance, float direction)
        {
            this.Position.SetDirection(direction);
            this.Position.Move(distance);
            this.HasChanges();
        }
        
        public override bool Equals(object obj)
        {
            if (obj is Pixel)
            {
                Pixel _obj = (Pixel)obj;

                return _obj == this;
            }
            else
            {
                return false;
            }
        }

        public bool Equals(Pixel other)
        {
            if (other == null) return false;
            return (this.GetHashCode().Equals(other.GetHashCode()));
        }

        public override int GetHashCode()
        {
            return (int)(this.Position.X + this.Position.Y + this.Position.Z + this.Position.Direction + (int)this.Color) % Int32.MaxValue;
        }

        public override string ToString()
        {
            return $"( Position: {this.Position.ToString()}, Color: {this.Color.ToString()} )";
        }

        public string ToString(bool includeFullClassPath)
        {
			return includeFullClassPath ? $"{this.GetType().FullName}: {this.ToString()}" : this.ToString();
		}

        internal dxlib32_222.Vertex ToDxLib32Vertex()
        {
            dxlib32_222.Vertex v = new dxlib32_222.Vertex();

            v.X = (int)this.Position.X;
            v.Y = (int)this.Position.Y;
            v.Z = 0;
            v.Color = (int)this.Color;

            return v;
        }
        #endregion
    }
}
