﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core.Graphics.Primitives
{
    /// <summary>
    /// Primitive box.
    /// </summary>
    public class Box
    {
		#region Internal vars
		private dxlib32_222.dx_GFX_Class _graphics = GraphicsManager.Instance._dx_gfx_instance; 
		#endregion

		#region Properties
		public Point2D Position { get; set; }
        public Point2D Size { get; set; }
        public Color BorderColor { get; set; }
        public Color FillColor { get; set; }
        #endregion

        #region Constructors
        public Box()
        {
            this.Position = this.Size = Point2D.Zero;
            this.BorderColor = this.FillColor = 0;
        }

        public Box(Point2D position, Point2D size, Color border, Color fill)
        {
            this.Position = position;
            this.Size = size;
            this.BorderColor = border;
            this.FillColor = fill;
        }
        #endregion

        #region Methods & functions
        public void Draw()
        {
            this._graphics.DRAW_Box((int)this.Position.X, (int)this.Position.Y,
									(int)(this.Position.X + this.Size.X), 
									(int)(this.Position.Y + this.Size.Y),
									0, (int)this.BorderColor, 
									this.FillColor == Color.Transparent ? false : true, 
									(int)this.FillColor);
        }
        
        public override string ToString()
        {
			return $"( Position: {this.Position.ToString()}, Size: ( Width: {this.Size.X}, Height: {this.Size.Y} ), Border Color: {this.BorderColor.ToString()}, Fill Color: {this.FillColor.ToString()} )";
		}

        public string ToString(bool includeFullClassPath)
        {
			return includeFullClassPath ? $"{this.GetType().FullName}: {this.ToString()}" : this.ToString();
		}
        #endregion
    }
}
