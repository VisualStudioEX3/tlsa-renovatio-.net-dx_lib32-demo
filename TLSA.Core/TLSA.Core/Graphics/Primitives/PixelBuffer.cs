﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TLSA.Engine.Core.Graphics.Primitives
{
    /// <summary>
    /// Define a buffer of pixels for advance working with large ammount of pixels.
    /// </summary>
    public class PixelBuffer : IEnumerable
    {
		#region Internal vars
		private dxlib32_222.dx_GFX_Class _graphics = GraphicsManager.Instance._dx_gfx_instance;
		private dxlib32_222.Vertex[] _array;
        private List<Pixel> _buffer;
        internal bool _hasChanges = false; 
        #endregion

        #region Properties
        public Pixel this[int index]
        {
            get { return this._buffer[index]; }
            set
            {
                this._buffer[index] = value;
                this._hasChanges = true;
            }
        }

        public int Count { get { return this._buffer.Count; } }
        #endregion

        #region Constructors
        public PixelBuffer()
        {
            this._array = new dxlib32_222.Vertex[0];
            this._buffer = new List<Pixel>();
        }
        #endregion

        #region Destructors
        public void Dispose()
        {
            this._array = null;
            this._buffer = null;
        } 
        #endregion

        #region Methods & functions
        public void Update()
        {
            if (this._hasChanges)
            {
                this._array = this._buffer.Select(e => e.ToDxLib32Vertex()).ToArray();
                this._hasChanges = false;
            }
        }

        public void Draw()
        {
            this._graphics.DRAW_Pixels(this._array);
        }

        public void Add(Point2D position, Color color)
        {
            this.Add(new Pixel(position, color));
        }

        public void Add(Pixel pixel)
        {
            pixel._pixelBufferOwner = this;
            this._buffer.Add(pixel);
            this._hasChanges = true;
        }

        public void Clear()
        {
            this._buffer.Clear();
            this._array = new dxlib32_222.Vertex[0];
        }

        public bool Contains(Point2D position)
        {
            return this._buffer.Exists(e => e.Position == position);
        }

        public bool Contains(Pixel pixel)
        {
            return this._buffer.Contains(pixel);
        }

        public void Remove(Point2D position)
        {
            this._buffer.RemoveAt(this._buffer.FindIndex(e => e.Position == position));
            this._hasChanges = true;
        }

        public void Remove(Pixel pixel)
        {
            this._buffer.Remove(pixel);
            this._hasChanges = true;
        }

        public void Remove(int index)
        {
            this._buffer.RemoveAt(index);
            this._hasChanges = true;
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)_buffer).GetEnumerator();
        }

        public ReadOnlyCollection<Pixel> AsReadOnly()
        {
            return this._buffer.AsReadOnly();
        }

        public List<Pixel> ToList()
        {
            return this._buffer.ToList();
        }

        public Pixel[] ToArray()
        {
            return this._buffer.ToArray();
        }

        public Dictionary<Point2D, Color> ToDictionary()
        {
            return this._buffer.ToDictionary(k => k.Position, v => v.Color);
        }

        public override string ToString()
        {
            return $"( Count: {this.Count} )";
        }

        public string ToString(bool includeFullClassPath)
        {
			return includeFullClassPath ? $"{this.GetType().FullName}: {this.ToString()}" : this.ToString();
		}
        #endregion
    }
}
