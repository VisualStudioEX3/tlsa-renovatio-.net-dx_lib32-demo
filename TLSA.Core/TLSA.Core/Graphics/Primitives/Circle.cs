﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core.Graphics.Primitives
{
    /// <summary>
    /// Primitive circle.
    /// </summary>
    public class Circle : IDisposable
    {
		#region Internal vars
		private dxlib32_222.dx_GFX_Class _graphics = GraphicsManager.Instance._dx_gfx_instance;
		private int _id = -1;
        private Point2D _position;
        private float _radius;
        private int _steps;
        private Color _border;
        private Color _fill;
        private bool _hasChanges = false;
        #endregion

        #region Properties
        /// <summary>
        /// Get or set the center of the circle.
        /// </summary>
        public Point2D Position
        {
            get { return this._position; }
            set { this._position = value; this._hasChanges = true; }
        }

        public float Radius
        {
            get { return this._radius; }
            set { this._radius = value; this._hasChanges = true; }
        }

        /// <summary>
        /// Define the steps to draw de circle.
        /// </summary>
        /// <remarks>This value determine how is drawing the circle. A step value 1, draw the circle pixel per pixel. A step value over 1, draw the circle using segments with lenght equal the step value.</remarks>
        public int Steps
        {
            get { return this._steps; }
            set { this._steps = value; this._hasChanges = true; }
        }

        public Color BorderColor
        {
            get { return this._border; }
            set { this._border = value; this._hasChanges = true; }
        }

        public Color FillColor
        {
            get { return this._fill; }
            set { this._fill = value; this._hasChanges = true; }
        }
        #endregion

        #region Constructors
        public Circle()
        {
            this.Position = Point2D.Zero;
            this.Radius = this.Steps = 0;
            this.BorderColor = this.FillColor = Color.Transparent;
        }

        public Circle(Point2D position, int radius, int steps, Color border, Color fill)
        {
            this.Position = position;
            this.Radius = radius;
            this.Steps = steps;
            this.BorderColor = border;
            this.FillColor = fill;
        }
        #endregion

        #region Methods & functions
        /// <summary>
        /// Update changes on the primitive.
        /// </summary>
        /// <remarks>Draw a circle require a high cost frame by frame. dx_lib32 precalculate the final circle and store in memory for future draw calls. Call this method for any change setted in this object.</remarks>
        public void Update()
        {
            if (this._id == -1 || this._hasChanges)
            {
                this._graphics.PRECAL_DeleteCircle(this._id);
                this._id = this._graphics.PRECAL_NewCircle((int)this._position.X, (int)this._position.Y,
                                                           (int)this._radius, this._steps,
                                                           this._border.ToInt32(),
                                                           this._fill == Color.Transparent ? false : true,
                                                           this._fill.ToInt32());
            }
        }

        public void Draw()
        {
            this._graphics.DRAW_Circle(this._id, 0);
        }

        public void Dispose()
        {
            this._graphics.PRECAL_DeleteCircle(this._id);
        }
        
        public override string ToString()
        {
			return $"( Position: {this.Position.ToString()}, Radius: {this.Radius}, Steps: {this.Steps}, Border Color: {this.BorderColor.ToString()}, Fill Color: {this.FillColor.ToString()} )";
		}

        public string ToString(bool includeFullClassPath)
        {
			return includeFullClassPath ? $"{this.GetType().FullName}: {this.ToString()}" : this.ToString();
		}
        #endregion
    }
}
