﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core.Graphics
{
    /// <summary>
    /// Render Target.
    /// </summary>
    /// <remarks>
    /// NOTE: dx_lib32 works with DirectX8. Direct3D8 only works with power 2 size textures.
    /// If load a image with non power 2 size, Direct3D8 resize to near power 2 size the result texture:
    /// 
    /// Example: 96x180 = 128x256
    /// 
    /// The Source_Width and Source_Height return the size of the original image.
    /// </remarks>
    public class RenderTarget : IDisposable
    {
		#region Internal vars
		private dxlib32_222.dx_GFX_Class _graphics = GraphicsManager.Instance._dx_gfx_instance;
		internal int _id;
        private Texture _texture;
        #endregion

        #region Properties
        public int Width { get { return this._texture.Width; } }
        public int Height { get { return this._texture.Height; } }
        public int Source_Width { get { return this._texture.Source_Width; } }
        public int Source_Height { get { return this._texture.Source_Height; } }
        #endregion

        #region Constructors & destructors
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="width">Power2 width.</param>
        /// <param name="height">Power2 height.</param>
        public RenderTarget(int width, int height)
        {
            try
            {
                this._texture = new Texture(width, height, true);
                this._id = this._graphics.TARGET_Create(this._texture._id);
                if (this._id != (int)dxlib32_222.GFX_ErrorCodes.GFX_OK)
                {
                    throw new Exception("Error to create Render Target.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error to create Render Target: {0}", ex.Message));
            }
        }
        
        public void Dispose()
        {
            this._graphics.TARGET_Close();
            this._graphics.TARGET_Destroy(this._id);
            this._texture.Dispose();
        }
        #endregion

        #region Methods & functions
        /// <summary>
        /// Open render target to accept draw calls.
        /// </summary>
        public void Open()
        {
            this._graphics.TARGET_Open(this._id);
        }

        /// <summary>
        /// Close render target and save result in the associate texture.
        /// </summary>
        public void Close()
        {
            this._graphics.TARGET_Close();
        }

        /// <summary>
        /// Return the texture with the result of this render target.
        /// </summary>
        /// <returns></returns>
        public Texture ToTexture()
        {
            return this._texture;
        }

        public override string ToString()
        {
            return $"( Texture: {this._texture.ToString()} )";
        }

        public string ToString(bool includeFullClassPath)
        {
			return includeFullClassPath ? $"{this.GetType().FullName}: {this.ToString()}" : this.ToString();
		}
        #endregion
    }
}
