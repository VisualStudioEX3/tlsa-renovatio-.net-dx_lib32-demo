﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dxlib32_222;

namespace TLSA.Engine.Core.Graphics
{
    /// <summary>
    /// ARGB color structure.
    /// </summary>
    public struct Color
    {
        #region Internal vars
        private dxlib32_222.dx_GFX_Class _graphics;
        private int _value;
        #endregion

        #region Properties
        public byte A { get; private set; }
        public byte R { get; private set; }
        public byte G { get; private set; }
        public byte B { get; private set; }
        #endregion

        #region Constants
        public static Color Transparent { get; private set; }
        public static Color AliceBlue { get; private set; }
        public static Color AntiqueWhite { get; private set; }
        public static Color Aqua { get; private set; }
        public static Color Aquamarine { get; private set; }
        public static Color Azure { get; private set; }
        public static Color Beige { get; private set; }
        public static Color Bisque { get; private set; }
        public static Color Black { get; private set; }
        public static Color BlanchedAlmond { get; private set; }
        public static Color Blue { get; private set; }
        public static Color BlueViolet { get; private set; }
        public static Color Brown { get; private set; }
        public static Color Burlywood { get; private set; }
        public static Color CadetBlue { get; private set; }
        public static Color Chartreuse { get; private set; }
        public static Color Chocolate { get; private set; }
        public static Color Coral { get; private set; }
        public static Color CornflowerBlue { get; private set; }
        public static Color Cornsilk { get; private set; }
        public static Color Crimson { get; private set; }
        public static Color Cyan { get; private set; }
        public static Color DarkBlue { get; private set; }
        public static Color DarkCyan { get; private set; }
        public static Color DarkGoldenrod { get; private set; }
        public static Color DarkGray { get; private set; }
        public static Color DarkGreen { get; private set; }
        public static Color DarkKhaki { get; private set; }
        public static Color DarkMagenta { get; private set; }
        public static Color DarkOliveGreen { get; private set; }
        public static Color DarkOrange { get; private set; }
        public static Color DarkOrchid { get; private set; }
        public static Color DarkRed { get; private set; }
        public static Color DarkSalmon { get; private set; }
        public static Color DarkSeaGreen { get; private set; }
        public static Color DarkSlateBlue { get; private set; }
        public static Color DarkSlateGray { get; private set; }
        public static Color DarkTurquoise { get; private set; }
        public static Color DarkViolet { get; private set; }
        public static Color DeepPink { get; private set; }
        public static Color DeepSkyBlue { get; private set; }
        public static Color DimGray { get; private set; }
        public static Color DodgerBlue { get; private set; }
        public static Color FireBrick { get; private set; }
        public static Color FloralWhite { get; private set; }
        public static Color ForestGreen { get; private set; }
        public static Color Fuchsia { get; private set; }
        public static Color Gainsboro { get; private set; }
        public static Color GhostWhite { get; private set; }
        public static Color Gold { get; private set; }
        public static Color Goldenrod { get; private set; }
        public static Color Gray { get; private set; }
        public static Color Green { get; private set; }
        public static Color GreenYellow { get; private set; }
        public static Color Honeydew { get; private set; }
        public static Color HotPink { get; private set; }
        public static Color IndianRed { get; private set; }
        public static Color Indigo { get; private set; }
        public static Color Ivory { get; private set; }
        public static Color Khaki { get; private set; }
        public static Color Lavender { get; private set; }
        public static Color Lavenderblush { get; private set; }
        public static Color LawnGreen { get; private set; }
        public static Color LemonChiffon { get; private set; }
        public static Color LightBlue { get; private set; }
        public static Color LightCoral { get; private set; }
        public static Color LightCyan { get; private set; }
        public static Color LightGoldenodYellow { get; private set; }
        public static Color LightGray { get; private set; }
        public static Color LightGreen { get; private set; }
        public static Color LightPink { get; private set; }
        public static Color LightSalmon { get; private set; }
        public static Color LightSeaGreen { get; private set; }
        public static Color LightSkyBlue { get; private set; }
        public static Color LightSlateGray { get; private set; }
        public static Color LightSteelBlue { get; private set; }
        public static Color LightYellow { get; private set; }
        public static Color Lime { get; private set; }
        public static Color LimeGreen { get; private set; }
        public static Color Linen { get; private set; }
        public static Color Magenta { get; private set; }
        public static Color Maroon { get; private set; }
        public static Color MediumAquamarine { get; private set; }
        public static Color MediumBlue { get; private set; }
        public static Color MediumOrchid { get; private set; }
        public static Color MediumPurple { get; private set; }
        public static Color MediumSeaGreen { get; private set; }
        public static Color MediumSlateBlue { get; private set; }
        public static Color MediumSpringGreen { get; private set; }
        public static Color MediumTurquoise { get; private set; }
        public static Color MediumVioletRed { get; private set; }
        public static Color MidnightBlue { get; private set; }
        public static Color Mintcream { get; private set; }
        public static Color MistyRose { get; private set; }
        public static Color Moccasin { get; private set; }
        public static Color NavajoWhite { get; private set; }
        public static Color Navy { get; private set; }
        public static Color OldLace { get; private set; }
        public static Color Olive { get; private set; }
        public static Color Olivedrab { get; private set; }
        public static Color Orange { get; private set; }
        public static Color Orangered { get; private set; }
        public static Color Orchid { get; private set; }
        public static Color PaleGoldenrod { get; private set; }
        public static Color PaleGreen { get; private set; }
        public static Color PaleTurquoise { get; private set; }
        public static Color PaleVioletred { get; private set; }
        public static Color PapayaWhip { get; private set; }
        public static Color PeachPuff { get; private set; }
        public static Color Peru { get; private set; }
        public static Color Pink { get; private set; }
        public static Color Plum { get; private set; }
        public static Color PowderBlue { get; private set; }
        public static Color Purple { get; private set; }
        public static Color Red { get; private set; }
        public static Color RosyBrown { get; private set; }
        public static Color RoyalBlue { get; private set; }
        public static Color SaddleBrown { get; private set; }
        public static Color Salmon { get; private set; }
        public static Color SandyBrown { get; private set; }
        public static Color SeaGreen { get; private set; }
        public static Color Seashell { get; private set; }
        public static Color Sienna { get; private set; }
        public static Color Silver { get; private set; }
        public static Color SkyBlue { get; private set; }
        public static Color SlateBlue { get; private set; }
        public static Color SlateGray { get; private set; }
        public static Color Snow { get; private set; }
        public static Color SpringGreen { get; private set; }
        public static Color SteelBlue { get; private set; }
        public static Color Tan { get; private set; }
        public static Color Teal { get; private set; }
        public static Color Thistle { get; private set; }
        public static Color Tomato { get; private set; }
        public static Color Turquoise { get; private set; }
        public static Color Violet { get; private set; }
        public static Color Wheat { get; private set; }
        public static Color White { get; private set; }
        public static Color WhiteSmoke { get; private set; }
        public static Color Yellow { get; private set; }
        public static Color YellowGreen { get; private set; }
        #endregion

        internal static void InitializeColorConstants()
        {
            Color.Transparent = new Color(0, 0, 0, 0);

            // The following list is based on this source: 
            // https://gist.github.com/LotteMakesStuff/f7ce43f11e545a151b95b5e87f76304c

            // NOTE: The follwing color names come from the CSS3 specification, Section 4.3 Extended Color Keywords
            // http://www.w3.org/TR/css3-color/#svg-color

            Color.AliceBlue = new Color(255, 240, 248, 255);
            Color.AntiqueWhite = new Color(255, 250, 235, 215);
            Color.Aqua = new Color(255, 0, 255, 255);
            Color.Aquamarine = new Color(255, 127, 255, 212);
            Color.Azure = new Color(240, 255, 255, 255);
            Color.Beige = new Color(245, 245, 220, 255);
            Color.Bisque = new Color(255, 228, 196, 255);
            Color.Black = new Color(255, 0, 0, 0);
            Color.BlanchedAlmond = new Color(255, 255, 235, 205);
            Color.Blue = new Color(255, 0, 0, 255);
            Color.BlueViolet = new Color(255, 138, 43, 226);
            Color.Brown = new Color(255, 165, 42, 42);
            Color.Burlywood = new Color(255, 222, 184, 135);
            Color.CadetBlue = new Color(255, 95, 158, 160);
            Color.Chartreuse = new Color(255, 127, 255, 0);
            Color.Chocolate = new Color(255, 210, 105, 30);
            Color.Coral = new Color(255, 255, 127, 80);
            Color.CornflowerBlue = new Color(255, 100, 149, 237);
            Color.Cornsilk = new Color(255, 255, 248, 220);
            Color.Crimson = new Color(255, 220, 20, 60);
            Color.Cyan = new Color(255, 0, 255, 255);
            Color.DarkBlue = new Color(255, 0, 0, 139);
            Color.DarkCyan = new Color(255, 0, 139, 139);
            Color.DarkGoldenrod = new Color(255, 184, 134, 11);
            Color.DarkGray = new Color(255, 169, 169, 169);
            Color.DarkGreen = new Color(255, 0, 100, 0);
            Color.DarkKhaki = new Color(255, 189, 183, 107);
            Color.DarkMagenta = new Color(255, 139, 0, 139);
            Color.DarkOliveGreen = new Color(255, 85, 107, 47);
            Color.DarkOrange = new Color(255, 255, 140, 0);
            Color.DarkOrchid = new Color(255, 153, 50, 204);
            Color.DarkRed = new Color(255, 139, 0, 0);
            Color.DarkSalmon = new Color(255, 233, 150, 122);
            Color.DarkSeaGreen = new Color(255, 143, 188, 143);
            Color.DarkSlateBlue = new Color(255, 72, 61, 139);
            Color.DarkSlateGray = new Color(255, 47, 79, 79);
            Color.DarkTurquoise = new Color(255, 0, 206, 209);
            Color.DarkViolet = new Color(255, 148, 0, 211);
            Color.DeepPink = new Color(255, 255, 20, 147);
            Color.DeepSkyBlue = new Color(255, 0, 191, 255);
            Color.DimGray = new Color(255, 105, 105, 105);
            Color.DodgerBlue = new Color(255, 30, 144, 255);
            Color.FireBrick = new Color(255, 178, 34, 34);
            Color.FloralWhite = new Color(255, 255, 250, 240);
            Color.ForestGreen = new Color(255, 34, 139, 34);
            Color.Fuchsia = new Color(255, 255, 0, 255);
            Color.Gainsboro = new Color(255, 220, 220, 220);
            Color.GhostWhite = new Color(255, 248, 248, 255);
            Color.Gold = new Color(255, 255, 215, 0);
            Color.Goldenrod = new Color(255, 218, 165, 32);
            Color.Gray = new Color(255, 128, 128, 128);
            Color.Green = new Color(255, 0, 128, 0);
            Color.GreenYellow = new Color(255, 173, 255, 47);
            Color.Honeydew = new Color(255, 240, 255, 240);
            Color.HotPink = new Color(255, 255, 105, 180);
            Color.IndianRed = new Color(255, 205, 92, 92);
            Color.Indigo = new Color(255, 75, 0, 130);
            Color.Ivory = new Color(255, 255, 255, 240);
            Color.Khaki = new Color(255, 240, 230, 140);
            Color.Lavender = new Color(255, 230, 230, 250);
            Color.Lavenderblush = new Color(255, 255, 240, 245);
            Color.LawnGreen = new Color(255, 124, 252, 0);
            Color.LemonChiffon = new Color(255, 255, 250, 205);
            Color.LightBlue = new Color(255, 173, 216, 230);
            Color.LightCoral = new Color(255, 240, 128, 128);
            Color.LightCyan = new Color(255, 224, 255, 255);
            Color.LightGoldenodYellow = new Color(255, 250, 250, 210);
            Color.LightGray = new Color(255, 211, 211, 211);
            Color.LightGreen = new Color(255, 144, 238, 144);
            Color.LightPink = new Color(255, 255, 182, 193);
            Color.LightSalmon = new Color(255, 255, 160, 122);
            Color.LightSeaGreen = new Color(255, 32, 178, 170);
            Color.LightSkyBlue = new Color(255, 135, 206, 250);
            Color.LightSlateGray = new Color(255, 119, 136, 153);
            Color.LightSteelBlue = new Color(255, 176, 196, 222);
            Color.LightYellow = new Color(255, 255, 255, 224);
            Color.Lime = new Color(255, 0, 255, 0);
            Color.LimeGreen = new Color(255, 50, 205, 50);
            Color.Linen = new Color(255, 250, 240, 230);
            Color.Magenta = new Color(255, 255, 0, 255);
            Color.Maroon = new Color(255, 128, 0, 0);
            Color.MediumAquamarine = new Color(255, 102, 205, 170);
            Color.MediumBlue = new Color(255, 0, 0, 205);
            Color.MediumOrchid = new Color(255, 186, 85, 211);
            Color.MediumPurple = new Color(255, 147, 112, 219);
            Color.MediumSeaGreen = new Color(255, 60, 179, 113);
            Color.MediumSlateBlue = new Color(255, 123, 104, 238);
            Color.MediumSpringGreen = new Color(255, 0, 250, 154);
            Color.MediumTurquoise = new Color(255, 72, 209, 204);
            Color.MediumVioletRed = new Color(255, 199, 21, 133);
            Color.MidnightBlue = new Color(255, 25, 25, 112);
            Color.Mintcream = new Color(255, 245, 255, 250);
            Color.MistyRose = new Color(255, 255, 228, 225);
            Color.Moccasin = new Color(255, 255, 228, 181);
            Color.NavajoWhite = new Color(255, 255, 222, 173);
            Color.Navy = new Color(255, 0, 0, 128);
            Color.OldLace = new Color(255, 253, 245, 230);
            Color.Olive = new Color(255, 128, 128, 0);
            Color.Olivedrab = new Color(255, 107, 142, 35);
            Color.Orange = new Color(255, 255, 165, 0);
            Color.Orangered = new Color(255, 255, 69, 0);
            Color.Orchid = new Color(255, 218, 112, 214);
            Color.PaleGoldenrod = new Color(255, 238, 232, 170);
            Color.PaleGreen = new Color(255, 152, 251, 152);
            Color.PaleTurquoise = new Color(255, 175, 238, 238);
            Color.PaleVioletred = new Color(255, 219, 112, 147);
            Color.PapayaWhip = new Color(255, 255, 239, 213);
            Color.PeachPuff = new Color(255, 255, 218, 185);
            Color.Peru = new Color(255, 205, 133, 63);
            Color.Pink = new Color(255, 255, 192, 203);
            Color.Plum = new Color(255, 221, 160, 221);
            Color.PowderBlue = new Color(255, 176, 224, 230);
            Color.Purple = new Color(255, 128, 0, 128);
            Color.Red = new Color(255, 255, 0, 0);
            Color.RosyBrown = new Color(255, 188, 143, 143);
            Color.RoyalBlue = new Color(255, 65, 105, 225);
            Color.SaddleBrown = new Color(255, 139, 69, 19);
            Color.Salmon = new Color(255, 250, 128, 114);
            Color.SandyBrown = new Color(255, 244, 164, 96);
            Color.SeaGreen = new Color(255, 46, 139, 87);
            Color.Seashell = new Color(255, 255, 245, 238);
            Color.Sienna = new Color(255, 160, 82, 45);
            Color.Silver = new Color(255, 192, 192, 192);
            Color.SkyBlue = new Color(255, 135, 206, 235);
            Color.SlateBlue = new Color(255, 106, 90, 205);
            Color.SlateGray = new Color(255, 112, 128, 144);
            Color.Snow = new Color(255, 255, 250, 250);
            Color.SpringGreen = new Color(255, 0, 255, 127);
            Color.SteelBlue = new Color(255, 70, 130, 180);
            Color.Tan = new Color(255, 210, 180, 140);
            Color.Teal = new Color(255, 0, 128, 128);
            Color.Thistle = new Color(255, 216, 191, 216);
            Color.Tomato = new Color(255, 255, 99, 71);
            Color.Turquoise = new Color(255, 64, 224, 208);
            Color.Violet = new Color(255, 238, 130, 238);
            Color.Wheat = new Color(255, 245, 222, 179);
            Color.White = new Color(255, 255, 255, 255);
            Color.WhiteSmoke = new Color(255, 245, 245, 245);
            Color.Yellow = new Color(255, 255, 255, 0);
            Color.YellowGreen = new Color(255, 154, 205, 50);
        }

        #region Constructors
        public Color(byte alpha, byte red, byte green, byte blue)
        {
            this._graphics = GraphicsManager.Instance._dx_gfx_instance;
            this._value = this.A = this.R = this.G = this.B = 0;
            this.Set(alpha, red, green, blue);
        }

        public Color(int color)
        {
            this._graphics = GraphicsManager.Instance._dx_gfx_instance;
            this._value = this.A = this.R = this.G = this.B = 0;
            this.Set(color);
        }
        #endregion

        #region Operators
        public static bool operator ==(Color a, Color b)
        {
            return a._value == b._value;
        }

        public static bool operator !=(Color a, Color b)
        {
            return !(a == b);
        }

        public static implicit operator Color(int value)
        {
            return new Color(value);
        }

        public static explicit operator int(Color value)
        {
            return value.ToInt32();
        }
        #endregion

        #region Methods & functions
        public void Set(byte alpha, byte red, byte green, byte blue)
        {
            this._value = this._graphics.ARGB_Set(alpha, red, green, blue);
            this.A = alpha; this.R = red; this.G = green; this.B = blue;
        }

        public void Set(int color)
        {
            var data = new ARGB();
            this._value = color;
            this._graphics.ARGB_Get(color, data);
            this.A = (byte)data.Alpha; this.R = (byte)data.Red; this.G = (byte)data.Green; this.B = (byte)data.Blue;
        }

        public override bool Equals(object obj)
        {
            if (obj is Color)
            {
                Color objColor = (Color)obj;
                return objColor == this;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return (int)(this.A + this.R + this.G + this.B) % Int32.MaxValue;
        }

        public int ToInt32()
        {
            return this._value;
        }

        public override string ToString()
        {
            return $"( Color: {this.ToInt32()} (Alpha: {this.A}, Red: {this.R}, Green: {this.G}, Blue: {this.B}) )";
        }

        public string ToString(bool includeFullClassPath)
        {
            return includeFullClassPath ? $"{this.GetType().FullName}: {this.ToString()}" : this.ToString();
        }
        #endregion
    }
}
