﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core.Graphics
{
    /// <summary>
    /// Sprite object. Draw a texture or portion of this with transformtations and visual effects.
    /// </summary>
    public class Sprite
    {
        #region Enums
        /// <summary>
        /// Mirror flip values.
        /// </summary>
        public enum SpriteMirror
        {
            None = dxlib32_222.Blit_Mirror.Mirror_None,
            Horizontal = dxlib32_222.Blit_Mirror.Mirror_Horizontal,
            Vertical = dxlib32_222.Blit_Mirror.Mirror_Vertical,
            Both = dxlib32_222.Blit_Mirror.Mirror_both
        }

        /// <summary>
        /// Sprite Effects (Non shader required)
        /// </summary>
        public enum SpriteFX
        {
            /// <summary>
            /// Default effect. Render using the sprite base color.
            /// </summary>
            Color = dxlib32_222.Blit_Alpha.Blendop_Color,
            /// <summary>
            /// Aditive effect. Pixels with color near to black are most transparents, pixels with color near to white are most solid.
            /// </summary>
            Aditive = dxlib32_222.Blit_Alpha.Blendop_Aditive,
            /// <summary>
            /// Sustrative effect. Similar to Additive but most darker.
            /// </summary>
            Sustrative = dxlib32_222.Blit_Alpha.Blendop_Sustrative,
            /// <summary>
            /// Inverse effect. Invert all colors.
            /// </summary>
            Inverse = dxlib32_222.Blit_Alpha.Blendop_Inverse,
            /// <summary>
            /// XOR effect. Inverse the texture pixel color respect the screen pixel color. Black always drawn as transparent color.
            /// </summary>
            XOR = dxlib32_222.Blit_Alpha.Blendop_XOR,
            /// <summary>
            /// Crystaline. Simulates a transluced cristal material. Black always drawn as solid color.
            /// </summary>
            Crystaline = dxlib32_222.Blit_Alpha.Blendop_Crystaline,
            /// <summary>
            /// GreyScale effect. Draws the texture in black & white. When applying alpha transparecy in the base sprite color, the black color always drawn as solid color.
            /// </summary>
            GreyScale = dxlib32_222.Blit_Alpha.Blendop_GreyScale
        }
		#endregion

		#region Internal vars
		private dxlib32_222.dx_GFX_Class _graphics = GraphicsManager.Instance._dx_gfx_instance;
		#endregion

		#region Properties
        /// <summary>
        /// Position to draw the sprite.
        /// </summary>
		public Point2D Position { get; set; }
        /// <summary>
        /// Rotation angle in degrees.
        /// </summary>
        public float Angle { get; set; }
        /// <summary>
        /// Texture used to draw with the sprite.
        /// </summary>
        public Texture Texture { get; set; }
        /// <summary>
        /// Base color used to render the sprite.
        /// </summary>
        public Color Color { get; set; }
        /// <summary>
        /// Mirror effect.
        /// </summary>
        public SpriteMirror Mirror { get; set; }
        /// <summary>
        /// Sprite effect based on blend operations.
        /// </summary>
        public SpriteFX Effect { get; set; }
        /// <summary>
        /// Define the anchor of the sprite, used to rotations and draw origin.
        /// </summary>
        public Point2D Anchor { get; set; }
        /// <summary>
        /// Force to draw centered. Ovewrite the Anchor property value.
        /// </summary>
        public bool ForceDrawCenter { get; set; }
        /// <summary>
        /// Define the region of the texture to render by the sprite. Empty frame draws the entire texture.
        /// </summary>
        public Rect Frame { get; set; }
        #endregion

        #region Constructor
        public Sprite(Texture texture = null)
        {
            this.Position = Point2D.Zero;
            this.Angle = 0f;
            this.Texture = texture;
            this.Color = Color.Transparent;
            this.Mirror = SpriteMirror.None;
            this.Effect = SpriteFX.Color;
            this.Frame = Rect.Empty;
            this.Anchor = Point2D.Zero;
            this.ForceDrawCenter = false;
        }
        #endregion

        #region Methods & Functions
        public void Draw()
        {
            if (this.Texture != null)
            {
                if (this.Frame != Rect.Empty)
                {
                    this._graphics.MAP_SetRegion(this.Texture._id, this.Frame.ToDxLib32GFXRect());
                }

                this._graphics.DEVICE_SetDrawCenter((int)this.Anchor.X, (int)this.Anchor.Y);

                this._graphics.DRAW_MapEx(this.Texture._id,
                                          (int)this.Position.X, (int)this.Position.Y, (int)this.Position.Z,
                                          0, 0,
                                          this.Angle,
                                          (dxlib32_222.Blit_Alpha)this.Effect,
                                          (int)this.Color,
                                          (dxlib32_222.Blit_Mirror)this.Mirror,
                                          (dxlib32_222.Blit_Filter)GraphicsManager.Instance.Filter,
                                          this.ForceDrawCenter); 
            }
        }

        /// <summary>
        /// Return the final vertex transformation of the sprite.
        /// </summary>
        /// <returns>Point2D array with the final vertex.</returns>
        /// <remarks>This function non render the sprite. Only calculate the final transformation.</remarks>
        public Point2D[] GetTransformVertex()
        {
            var vertex = new dxlib32_222.Vertex[4];
            var points = new Point2D[4];

            this._graphics.PRECAL_WriteSpriteTransformVertex();
            {
                this.Draw();
            }
            this._graphics.PRECAL_ReadSpriteTransformVertex(vertex[0], vertex[1], vertex[2], vertex[3]);

            for (int i = 0; i < 4; i++)
            {
                points[i].X = vertex[i].X;
                points[i].Y = vertex[i].Y;
                points[i].Z = vertex[i].Z;
            }

            return points;
        }

        public override string ToString()
        {
			return $"( Position: {this.Position}, Angle: {this.Angle}, Texture: {this.Texture}, Color: {this.Color}, Mirror: {this.Mirror}, Effect: {this.Effect}, Center: {this.Anchor}, ForceDrawCenter: {this.ForceDrawCenter}, Frame: {this.Frame})";
        }

        public string ToString(bool includeFullClassPath)
        {
			return includeFullClassPath ? $"{this.GetType().FullName}: {this.ToString()}" : this.ToString();
		}
        #endregion
    }
}
