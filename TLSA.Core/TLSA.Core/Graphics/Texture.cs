﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core.Graphics
{
    /// <summary>
    /// Texture.
    /// </summary>
    /// <remarks>
    /// NOTE: dx_lib32 works with DirectX8. Direct3D8 only works with power 2 size textures.
    /// If load an image with non power 2 size, Direct3D8 resize to near power 2 size the result texture:
    /// 
    /// Example: 96x180 = 128x256
    /// 
    /// The Source_Width and Source_Height return the size of the original image.
    /// </remarks>
    public class Texture : IDisposable
    {
		#region Internal vars
		private dxlib32_222.dx_GFX_Class _graphics = GraphicsManager.Instance._dx_gfx_instance;
		internal int _id;
        #endregion

        #region Properties
        public string Filename { get; internal set; }
        /// <summary>
        /// Indicates if the texture as loaded from memory, not from file.
        /// </summary>
        public bool LoadFromMemory { get; internal set; }
        /// <summary>
        /// Indicates if the texture is a render target texture.
        /// </summary>
        public bool IsRenderTarget { get; internal set; }
        public int Width { get; internal set; }
        public int Height { get; internal set; }
        public int Source_Width { get; internal set; }
        public int Source_Height { get; internal set; }
        public Point2D Anchor { get; set; }
        #endregion

        #region Constructor & destructor
        public Texture(string filename)
        {
            this._id = this._graphics.MAP_Load(filename, 0, false, false);
            if (this._id == (int)dxlib32_222.GFX_ErrorCodes.GFX_OK)
            {
                this.Filename = filename;
                this.LoadFromMemory = false;
                this.IsRenderTarget = false;
                this.Anchor = Point2D.Zero;
                GetInfo();
            }
            else
            {
                throw new Exception(string.Format("Error to load the texture '{0}'!", filename));
            }
        }

        public Texture(byte[] data)
        {
            this._id = this._graphics.MAP_LoadFromMemory(data, 0, false, false);
            if (this._id == (int)dxlib32_222.GFX_ErrorCodes.GFX_OK)
            {
                this.Filename = string.Empty;
                this.LoadFromMemory = true;
                this.IsRenderTarget = false;
                this.Anchor = Point2D.Zero;
                GetInfo();
            }
            else
            {
                throw new Exception(string.Format("Error to load the texture from the array data!"));
            }
        }

        public Texture(int width, int height, bool isRenderTarget)
        {
            this._id = this._graphics.MAP_Create(width, height, isRenderTarget);
            if (this._id == (int)dxlib32_222.GFX_ErrorCodes.GFX_OK)
            {
                this.Filename = string.Empty;
                this.LoadFromMemory = false;
                this.IsRenderTarget = isRenderTarget;
                this.Anchor = Point2D.Zero;
                GetInfo();
            }
            else
            {
                throw new Exception(string.Format("Error to create the texture!"));
            }
        }
        
        public void Dispose()
        {
            this._graphics.MAP_Unload(this._id);
        }
        #endregion

        #region Methods & functions
        private void GetInfo()
        {
            var info = new dxlib32_222.GFX_Info();
            this._graphics.MAP_GetInfo(this._id, info);

            this.Width = info.Width;
            this.Height = info.Height;
            this.Source_Width = info.Image_Width;
            this.Source_Height = info.Image_Height;
        }

        public override string ToString()
        {
			return $"( Filename: {this.Filename}, Load from memory: {this.LoadFromMemory}, Width: {this.Width}, Height: {this.Height}, Real_Width: {this.Source_Width}, Real_Height: {this.Source_Height} )";
        }

        public string ToString(bool includeFullClassPath)
        {
			return includeFullClassPath ? $"{this.GetType().FullName}: {this.ToString()}" : this.ToString();
		}
        #endregion
    }
}
