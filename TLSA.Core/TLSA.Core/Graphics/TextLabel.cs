﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core.Graphics
{
    /// <summary>
    /// Text label.
    /// </summary>
    public class TextLabel
    {
        #region Enumerations
        /// <summary>
        /// Text aligns.
        /// </summary>
        public enum TextAlign
        {
            Left = dxlib32_222.Text_Align.Align_Left,
            Right = dxlib32_222.Text_Align.Align_Right,
            Center = dxlib32_222.Text_Align.Align_Center
        }
		#endregion

		#region Internal vars
		private dxlib32_222.dx_GFX_Class _graphics = GraphicsManager.Instance._dx_gfx_instance;
		#endregion

		#region Properties
		public Font Font { get; set; }
        public Color Color { get; set; }
        public string Text { get; set; }
        public Point2D Position { get; set; }
        public TextAlign Align { get; set; }
        #endregion

        #region Constructors
        public TextLabel(Font font, Color color, Point2D position, string text = "", TextAlign align = TextAlign.Left)
        {
            this.Font = font;
            this.Color = color;
            this.Position = position;
            this.Text = text;
            this.Align = align;
        }
        #endregion

        #region Methods & functions
        public void Draw()
        {
            this._graphics.DRAW_Text(this.Font._id, this.Text, (int)this.Position.X, (int)this.Position.Y, (int)this.Position.Z, this.Color.ToInt32(), (dxlib32_222.Text_Align)this.Align);
        }

        /// <summary>
        /// Return the bounds, with the position, of the current text label configuration.
        /// </summary>
        /// <returns>Return a Rect structure with the label text position and size.</returns>
        public Rect GetBounds()
        {
            var size = this.Font.GetStringSize(this.Text);
            return new Rect(this.Position.X, this.Position.Y, size.X, size.Y);
        }

        public override string ToString()
        {
			return $"( Font: {this.Font}, Color: {this.Color}, Text: {this.Text}, Position: {this.Position}, Align: {this.Align} )";
        }

        public string ToString(bool includeFullClassPath)
        {
			return includeFullClassPath ? $"{this.GetType().FullName}: {this.ToString()}" : this.ToString();
		}
        #endregion
    }
}
