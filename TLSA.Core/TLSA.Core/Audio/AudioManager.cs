﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dxlib32_222;

namespace TLSA.Engine.Core.Audio
{
	public class AudioManager : IDisposable
	{
		#region Singleton
		private static AudioManager _instance;

		public static AudioManager Instance
		{
			get
			{
				if (_instance == null) _instance = new AudioManager();
				return _instance;
			}
		}
		#endregion

		#region Internal vars
		internal dx_Sound_Class _dx_audio_instance;
		#endregion

		#region Constructor & destructor
		private AudioManager()
		{
            this._dx_audio_instance = new dx_Sound_Class();
        }

		public void Dispose()
		{
			this.CheckInitialization();
			this._dx_audio_instance.Terminate();
		}
		#endregion

		#region Methods & Functions
		private void CheckInitialization()
		{
			if (this._dx_audio_instance == null) throw new Exception("Manager not initialized.");
		}

		public void Initialize(int hWnd)
		{
            this.CheckInitialization();
			this._dx_audio_instance.Init(hWnd, 64);
		} 
		#endregion
	}
}
