﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dxlib32_222;

namespace TLSA.Engine.Core.Video
{
    public class VideoManager : IDisposable
    {
		#region Singleton
		private static VideoManager _instance;

		public static VideoManager Instance
		{
			get
			{
				if (_instance == null) _instance = new VideoManager();
				return _instance;
			}
		}
		#endregion

		#region Internal vars
		internal dx_Video_Class _dx_video_instance;
		#endregion

		#region Constructor & Destructor
		private VideoManager()
		{
            this._dx_video_instance = new dx_Video_Class();
		}

		public void Dispose()
		{
			this.CheckInitialization();
			_dx_video_instance.Terminate();
		}
		#endregion

		#region Methods & Functions
		private void CheckInitialization()
		{
			if (this._dx_video_instance == null) throw new Exception("Manager not initialized.");
		}

		public void Initialize(int hWnd)
		{
            this.CheckInitialization();			
			this._dx_video_instance.Init(hWnd);
		} 
		#endregion
	}
}
