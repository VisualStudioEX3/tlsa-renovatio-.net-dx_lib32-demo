﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dxlib32_222;

namespace TLSA.Engine.Core.Input
{
    public class InputManager : IDisposable
    {
		#region Singleton
		private static InputManager _instance;

		public static InputManager Instance
		{
			get
			{
				if (_instance == null) _instance = new InputManager();
				return _instance;
			}
		}
		#endregion

		#region Internal vars
		internal dx_Input_Class _dx_input_instance;
		#endregion

		#region Constructor & destructor
		private InputManager()
		{
            this._dx_input_instance = new dx_Input_Class();
        }

		public void Dispose()
		{
			this.CheckInitialization();
			this._dx_input_instance.Terminate();
		}
		#endregion

		#region Methods & Functions
		private void CheckInitialization()
		{
			if (this._dx_input_instance == null) throw new Exception("Manager not initialized.");
		}

		public void Initialize(int hWnd)
		{
            this.CheckInitialization();
			this._dx_input_instance.Init(hWnd);
		}

		public void Update()
		{
			this.CheckInitialization();
			this._dx_input_instance.Update();
		} 
		#endregion
	}
}
