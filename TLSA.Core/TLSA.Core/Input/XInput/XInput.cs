using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace TLSA.Engine.Core.Input.XInput
{
	public enum ControllerButtons: ushort
	{
		Up = 0x00000001,
        Down = 0x00000002,
		Left = 0x00000004,
		Right = 0x00000008,
		Start = 0x00000010,
		Back = 0x00000020,
		ThumbLeft = 0x00000040,
		ThumbRight = 0x00000080,
		ShoulderLeft = 0x0100,
		ShoulderRight = 0x0200,
		A = 0x1000,
		B = 0x2000,
		X = 0x4000,
		Y = 0x8000,

		None = 0
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct Vibration
	{
		public ushort LeftMotorSpeed;
		public ushort RightMotorSpeed;

		public Vibration(ushort left, ushort right)
		{
			LeftMotorSpeed = left;
			RightMotorSpeed = right;
		}
	} 

	[StructLayout(LayoutKind.Sequential)]
	public struct GamePad
	{
		public ControllerButtons Buttons;
		public byte LeftTrigger;
		public byte RightTrigger;
		public short ThumbLeftX;
		public short ThumbLeftY;
		public short ThumbRightX;
		public short ThumbRightY;
	}

	
	[StructLayout(LayoutKind.Sequential)]
	public struct State
	{
		public uint PacketNumber;
		public GamePad Gamepad;
	} 

	public class ControllerCollection
	{
		private ArrayList _controllers = new ArrayList();	
		
		public ControllerCollection()
		{
			for (int i = 0; i < 4; i++)
			{
				_controllers.Add( new Controller( i ) );
			}
		}

		public Controller this[int index]
		{
			get
			{
				return (Controller)this._controllers[index];
			}
		}
	}

	public class XInput
	{
		public const int ERROR_SUCCESS = 0;

		private XInput()
		{
			// no instanciation
		}

		private static ControllerCollection _controllerList = new ControllerCollection();
		public static ControllerCollection Controllers
		{
			get { return _controllerList; }
		}
		
		//[System.Security.SuppressUnmanagedCodeSecurity] // We won't use this maliciously
		[DllImport("xinput9_1_0.dll", EntryPoint="XInputSetState", CharSet=CharSet.Auto, CallingConvention=CallingConvention.Winapi)]
		internal static extern int SetState(int dwUserIndex, ref Vibration pVibration);

		
		//[System.Security.SuppressUnmanagedCodeSecurity] // We won't use this maliciously
		[DllImport("xinput9_1_0.dll", EntryPoint="XInputGetState", CharSet=CharSet.Auto, CallingConvention=CallingConvention.Winapi)]
		internal static extern int GetState(int dwUserIndex, ref State pState);
		
		
	}
}
