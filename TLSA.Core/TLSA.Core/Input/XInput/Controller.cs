using System;

namespace TLSA.Engine.Core.Input.XInput
{
	/// <summary>
	/// Summary description for Controller.
	/// </summary>
	public class Controller
	{
		private static bool _automaticallyPollState = true;

		/// <summary>
		/// May be used to turn automatic state polling on the controller on and off.
		/// If it's turned on (set to true, which is the default setting), each call 
		/// to access data on the controller object will polly the controller for the
		/// latest state. 
		/// 
		/// This may be redundant, so you can turn of this statis switch by setting it 
		/// to false. This means you will have to poll the controller state manually by
		/// calling the PollState() method. This could be done at the beginning of each
		/// frame for example, to prevent redundant polls to the controller
		/// 
		/// </summary>
		public static bool AutomaticallyPollState
		{
			get
			{
				return _automaticallyPollState;
			}
			set
			{
				_automaticallyPollState = value;
			}
		}

		// internal data structures to hold the data for a controller
		private int _playerIndex = 0;
		private State _state = new State();
		private Vibration _vibration = new Vibration();

		// only internal access to this class
		internal Controller(int playerIndex)
		{
			this._playerIndex = playerIndex;
		}

		/// <summary>
		/// Retrieves the current state of a controller by polling it using the XInput.GetState
		/// function. The result from this poll is stored in the internal State struct, which can
		/// be retrieved through this controller's State property.
		/// </summary>
		/// <returns>A boolean whether or not the poll was successful</returns>
		public bool PollState()
		{
			// this may be limited to once per frame or to a specific time period, using
			// AutomaticallyPollState to disable automatic polling through other accessors
			return XInput.GetState(this._playerIndex, ref this._state) == XInput.ERROR_SUCCESS;
		}

		private bool PollStateInternal()
		{
			if (AutomaticallyPollState)
			{
				return PollState();
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Checks whether data can be polled from the controller with the specified player index.
		/// If data cannot be polled, the controller typically is not connected.
		/// </summary>
		public bool IsConnected
		{
			get
			{
				return PollState();
			}
		}

		public ushort LeftMotorSpeed
		{
			get
			{
				return _vibration.LeftMotorSpeed;
			}
			set
			{
				_vibration.LeftMotorSpeed = value;
				XInput.SetState(this._playerIndex, ref this._vibration);
			}
		}

		public ushort RightMotorSpeed
		{
			get
			{
				return _vibration.RightMotorSpeed;
			}
			set
			{
				_vibration.RightMotorSpeed = value;
				XInput.SetState(this._playerIndex, ref this._vibration);
			}
		}

		public State State
		{
			get
			{
				PollStateInternal();
				return _state;
			}
		}

		public bool IsButtonPressed( ControllerButtons buttonsToCheck )
		{
			PollStateInternal();
			return (this._state.Gamepad.Buttons & buttonsToCheck) != 0;
		}

		public void SetMotorSpeeds(ushort leftMotor, ushort rightMotor)
		{			
			_vibration.LeftMotorSpeed = leftMotor;
			_vibration.RightMotorSpeed = rightMotor;
			XInput.SetState(this._playerIndex, ref this._vibration);
		}
	}
}
