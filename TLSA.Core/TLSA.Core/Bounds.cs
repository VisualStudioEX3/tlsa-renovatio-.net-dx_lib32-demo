﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSA.Engine.Core
{
    /// <summary>
    /// Bounds structure.
    /// </summary>
    public struct Bounds
    {
        #region Fields
        public float Left;
        public float Top;
        public float Right;
        public float Bottom;
        #endregion

        #region Constructors
        public Bounds(float left, float top, float right, float bottom)
        {
            this.Left = left; this.Top = top; this.Right = right; this.Bottom = bottom;
        }
        #endregion

        #region Operators
        public static bool operator ==(Bounds a, Bounds b)
        {
            return
            (
                MathHelper.Approximately(a.Left, b.Left) &&
                MathHelper.Approximately(a.Top, b.Top) &&
                MathHelper.Approximately(a.Right, b.Right) &&
                MathHelper.Approximately(a.Bottom, b.Bottom)
            );
        }

        public static bool operator !=(Bounds a, Bounds b)
        {
            return !(a == b);
        }
        #endregion

        #region Methods & Functions
        public override bool Equals(object obj)
        {
            if (obj is Bounds)
            {
                Bounds _obj = (Bounds)obj;

                return _obj == this;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return (int)(this.Left + this.Top + this.Right + this.Bottom) % Int32.MaxValue;
        }

        public override string ToString()
        {
			return $"( Left: {this.Left}, Top: {this.Top}, Right: {this.Right}, Bottom: {this.Bottom} )";
        }

        public string ToString(bool includeFullClassPath)
        {
			return includeFullClassPath ? $"{this.GetType().FullName}: {this.ToString()}" : this.ToString();
		}
        #endregion
    }
}
